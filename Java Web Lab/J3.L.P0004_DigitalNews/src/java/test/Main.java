/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import java.math.RoundingMode;
import java.text.DecimalFormat;

/**
 *
 * @author MSI
 */
public class Main {
    
    public static void main(String[] args) {
        DecimalFormat df = new DecimalFormat("0.00");
        df.setRoundingMode(RoundingMode.HALF_UP);
        System.out.println(df.format(1));
        System.out.println(df.format(1.34213));
        System.out.println(df.format(0.34213));
        System.out.println(df.format(0.34813));
    }
    
}
