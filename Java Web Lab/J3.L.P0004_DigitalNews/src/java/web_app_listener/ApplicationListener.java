package web_app_listener;

import db.DBContext;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Web application lifecycle listener.
 *
 * @author MSI
 */
public class ApplicationListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        System.out.println("Context Init!");
        try {
            DBContext.setUrl(getConfigValue("SqlConnection"));
            DBContext.setUser(getConfigValue("user"));
            DBContext.setPw(getConfigValue("pw"));
            DBContext.setImageDir(getConfigValue("imgDir"));
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Context init failed!");
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        System.out.println("Context destroyed!");
    }
    
    public String getConfigValue(String name) throws Exception {
        InitialContext init = new InitialContext();
        Context con = (Context) init.lookup("java:comp/env");
        return (String) con.lookup(name);
    }
}