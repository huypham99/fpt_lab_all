/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 *
 * @author MSI
 */
public class DBContext {

    private static String url;
    private static String user;
    private static String pw;
    private static String imageDir;

    public Connection getConnection() throws Exception {
        Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        return DriverManager.getConnection(url, user, pw);
    }

    public void closeConnection(Connection con, PreparedStatement ps, ResultSet rs) throws Exception {
        if (rs != null && !rs.isClosed()) {
            rs.close();
        }
        if (ps != null && !ps.isClosed()) {
            ps.close();
        }
        if (con != null && !con.isClosed()) {
            con.close();
        }
    }

    public static void setUrl(String url) {
        DBContext.url = url;
    }

    public static void setUser(String user) {
        DBContext.user = user;
    }

    public static void setPw(String pw) {
        DBContext.pw = pw;
    }

    public static void setImageDir(String imageDir) {
        DBContext.imageDir = imageDir;
    }

    public static String getImageDir() {
        return imageDir;
    }
    
}
