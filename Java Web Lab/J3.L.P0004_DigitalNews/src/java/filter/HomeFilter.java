/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filter;

import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author MSI
 */
@WebFilter(filterName = "HomeFilter", urlPatterns = {"/index.jsp","/header.jsp","/right-div.jsp"})
public class HomeFilter implements Filter {
    
    public HomeFilter() {
    }    
    
    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain)
            throws IOException, ServletException {
        
        HttpServletRequest httpReq = (HttpServletRequest)request;
        HttpServletResponse httpRes = (HttpServletResponse)response;
        System.out.println(httpReq.getServletPath());
//        httpRes.sendRedirect(httpReq.getContextPath()+ "/home");
//        httpReq.getRequestDispatcher(httpReq.getContextPath()+ "/home").forward(request, response);
//        httpReq.getRequestDispatcher(getBaseUrl(httpReq)+ "/home").forward(request, response);
        httpReq.getRequestDispatcher("/home").forward(httpReq, httpRes);
        try {
            chain.doFilter(request, response);
            System.out.println("gg");
        } catch (Throwable t) {
            t.printStackTrace();
            System.out.println("null null servlet");
            httpReq.getRequestDispatcher("error.jsp").forward(request, response);
        }
        
    }

    
    public void destroy() {        
    }

    /**
     * Init method for this filter
     */
    public void init(FilterConfig filterConfig) {        
    }
    
    private String getBaseUrl(HttpServletRequest req) {
        String scheme = req.getScheme() + "://";
        String serverName = req.getServerName();
        String port = ":"+req.getServerPort();
        String contextPath = req.getContextPath();
        return scheme + serverName + port + contextPath;
    }
    
}
