# Lab All Materials

Đây là tất cả bài lab được code trong laptop mình, do vậy sẽ có thể không đủ LOC để pass vì có 1 vài bài mình chỉ code trên máy phòng LAB thôi.
Link đề bài:
* [Lab C](https://drive.google.com/drive/u/1/folders/0B7rgF-vszrjbbXlVajRPaXA1Wk0)
* [Lab Java Core](https://drive.google.com/drive/folders/0B7rgF-vszrjbaEgyczhhTVpzX0E?fbclid=IwAR1UZlm9_q4ljO2qBpPovBOvfqFPr4QA2f7nusx8oCEqrzToeCQMlX59A2w)
* [Lab Java Desktop](https://drive.google.com/drive/u/0/folders/0B7rgF-vszrjbYV9lUGs0WkFjXzQ?fbclid=IwAR3aMFw-rq_StTbJgbHv7vYlUT7yVlVuluI3vvmo0XqQUXonKiUR1e6qdOg)
* [Lab Java Web](https://drive.google.com/drive/folders/1wF_D3vA0eOlauP9-mR0dYG28pNSVwftK)

## Notes quan trọng

### C Lab

Những bài của mình được code bằng **NetBeans IDE**, nhưng mình biết phần lớn các bạn đều vừa học xong **PRF192** và các thầy cho dùng **DevCpp** nên để chạy được bằng **DevCpp**, hãy thực hiện các bước sau:
1.  Vào thư mục của bài mình muốn chạy, mở file đuôi ***.cpp*** bằng **DevCpp**
2.  **Ctrl + Shift + R** để bật **Replace In Files Diaglog**
3.  Trong text field **Text to find**, nhập fpurge
4.  Trong text field **Replace with**, nhập fflush
5.  Ấn Replace
6.  Sau đấy DevCpp sẽ replace hết fpurge thành fflush, nếu nó báo "Search not found" thì có nghĩa là bài đó không cần đổi fpurge thành fflush nữa, fflush và fpurge đều dùng để xóa kí tự Enter thừa trong bộ nhớ đệm nhập từ bàn phím, nhưng fpurge mình dùng trên **NetBeans IDE**, còn fflush chạy trên **DevCpp**, tuy bản chất nó khác nhưng các bạn cứ hiểu nôm na là như thế đi (nhưng đừng giải thích với thầy như vậy nếu được hỏi nhé, chi tiết thì xin mời hỏi gg-sama nhé) :3.

### Mấy Lab khác ko có gì cần note.