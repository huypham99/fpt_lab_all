/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package j1.s.p0009;

/**
 *
 * @author MSI
 */
public class fibo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int ceilNumber = 45;
        int start = 0;
        int next = 1;
        System.out.println("The "+ceilNumber+" sequence Fibonacci:");
        System.out.println("1. "+start);
        System.out.println("2. "+next);
        int No = 3;
        displayFibo(No,start,next,ceilNumber);
    }
    static void displayFibo(int No, int lastOfLast,int last, int ceil) {
        int sum = lastOfLast+last;
        System.out.println(No+". "+sum);
        if(No==ceil) return;
        displayFibo(++No, last, sum, ceil);
    }
}