/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package j1.s.p0004.quicksort;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author MSI
 */
public class J1SP0004QuickSort {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int array[];
        int size;
        size = setSize();
        array = generateArray(size);
        System.out.print("Unsorted array: ");
        displayArray(array);
        //sort array by built-in function
//        array = new int[]{1, 12, 5, 26, 6, 14, 3, 7, 2};
//        bubble(array,array.length);
        quickSort(array,0,size-1);
        System.out.print("Sorted array: ");
        displayArray(array);
    }
    
    static void bubble(int[] array, int size) {
        int temp;
        for (int i = 1; i < size-1; i++) {
            for (int j = 0; j < size - i; j++) {
                if(array[j]>array[j+1]) {
                    temp = array[j+1];
                    array[j+1] = array[j];
                    array[j] = temp;
                }
            }
        }
    }
    
    static void quickSort(int[] array, int start, int end) {
        int pivot = (start + end)/2;
//        System.out.println("pivot: "+array[pivot]);
        int i, j;
        i = start;
        j = end;
        while(i <= j) {
            while(array[i]<array[pivot]) i++;
            while(array[pivot]<array[j]) j--;
//            displayArray(array);
            if(i<=j) {
                int temp = array[i];
                array[i] = array[j];
                array[j] = temp;
                i++;
                j--;
            }
        }
//        System.out.println("");
        if(start < i - 1 ) quickSort(array, start, i-1);
        if(i < end) quickSort(array, i, end);
    }
    
    
    static void displayArray(int[] array) {
        System.out.print("[");
        int size = array.length;
        for (int i = 0; i < size-1; i++) {
            System.out.print(array[i]+", ");
        }
        System.out.println(array[size-1]+"]");
    }
    
    static int[] generateArray(int size) {
        int[] array;
        array = new int[size];
        //generate random elements in array
        for (int i = 0; i < size; i++) {
            array[i] = Math.abs(new Random().nextInt())%(size+1);
        }
        return array;
    }
    
    static int setSize() {
        String size;
        //validate size of array
        do {
            System.out.println("Enter number of array:");
            size = sc.nextLine();
        }while(!checkSize(size));
        return Integer.parseInt(size);
    }
    
    static boolean checkSize(String sizeStr)  {
        if(!checkBlank(sizeStr)) return false;
        //if not a number
        if(!isInt(sizeStr)) {
            System.out.println("Please input a number.");
            return false;
        }
        //if searchVal is 0 or negative
        if(Integer.parseInt(sizeStr)<=0) {
            System.out.println("Size of matrix must greater than 0.");
            return false;
        }
        return true;
    }
    
    static boolean isInt(String string) {
        try {
            int temp = Integer.parseInt(string);
        }catch(NumberFormatException e) {
            return false;
        }
        return true;
    }
    
    static boolean checkBlank(String string) {
        if(string.trim().isEmpty()) {
            System.out.println("This cannot blank.");
            return false;
        }
        return true;
    }
    
    static Scanner sc = new Scanner(System.in);
}
