/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package j1.s.p0008;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.StringTokenizer;

/**
 *
 * @author MSI
 */
public class wordLetterCount {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter your content:");
        String input = sc.nextLine();
        wordLetterCount countThis = new wordLetterCount();
        countThis.countWord(input);
        countThis.countChar(input);
        countThis.displayResult();
    }
    
    private Map<String,Integer> wordCount = new HashMap<String,Integer>();
    private Map<Character,Integer> charCount = new HashMap<Character,Integer>();
    
    
    public void countWord(String input) {
        String[] sentence = input.split("\\s+");
        for (String word: sentence) {
            if(!wordCount.containsKey(word)) {
                wordCount.put(word, 1);
            }else {
                wordCount.put(word, wordCount.get(word)+1);
            }
        }
//        StringTokenizer spliter = new StringTokenizer(input);
//        while(spliter.hasMoreTokens()) {
//            String word = spliter.nextToken();
//            if(!wordCount.containsKey(word)) {
//                wordCount.put(word, 1);
//            }else {
//                wordCount.put(word, wordCount.get(word)+1);
//            }
//        }
    }
    
    public void countChar(String input) {
        for (Character c : input.toCharArray()) {
            if(c==' ') continue;
            if(!charCount.containsKey(c)) {
                charCount.put(c, 1);
            }else {
                charCount.put(c, charCount.get(c)+1);
            }
        }
    }


    public void displayResult() {
        System.out.println(wordCount);
        System.out.println(charCount);
    }
    
    
//-----------------------------------------------------------------------------
    
//    void countWord1(String input) {
//        String[] words = input.trim().split("\\s+");
//        ArrayList<String> wordsNotDup = new ArrayList();
//        for (int i = 0; i < words.length; i++) {
//            wordsNotDup.add(words[i]);
//        }
//        System.out.println(wordsNotDup.size());
//        for (int i = 0; i < wordsNotDup.size(); i++) {
//            for (int j = i+1; j < wordsNotDup.size(); j++) {
//                if(wordsNotDup.get(i).equals(wordsNotDup.get(j))) {
//                    wordsNotDup.remove(j);
//                    j--;
//                }
//            }
//        }
//        System.out.println(wordsNotDup.size());
//        int[]countWord = new int[wordsNotDup.size()];
//        for (int i = 0; i < countWord.length; i++) {
//            countWord[i]=0;
//        }
//        Counting(words,wordsNotDup,countWord);
//        DisplayResult(wordsNotDup,countWord);
//    }
//    void Counting(String[] words, ArrayList<String> wordsNotDup, int[] countWord) {
//        for (int i = 0; i < wordsNotDup.size(); i++) {
//            for (int j = 0; j < words.length; j++) {
//                if(wordsNotDup.get(i).equals(words[j])) countWord[i]++;
//            }
//        }
//    }
//    void DisplayResult(ArrayList<String> wordsNotDup, int[] countWord) {
//        System.out.print("{");
//        for (int i = 0; i < wordsNotDup.size()-1; i++) {
//            System.out.print(wordsNotDup.get(i)+"="+countWord[i]+", ");
//        }
//        System.out.println(wordsNotDup.get(countWord.length-1)+"="+countWord[countWord.length-1]+"}");
//    }
}
