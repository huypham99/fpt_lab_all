/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package j1.s.p0001.bubblesort;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author MSI
 */
public class bubbleSort {

    /**
     * @param args the command line arguments
     */
    static Scanner sc = new Scanner(System.in);
    public static void main(String[] args) {
        //size of array
        int size;
        boolean isInteger;
        //repeat if size not valid
        do {
            //check excpetion of integer value
            try {
                size = 1;
                isInteger = true;
                System.out.println("Enter size of array: ");
                size = Integer.parseInt(sc.nextLine());
            }catch(Exception e) {
                size = 1;
                isInteger = false;
                System.out.println("Integer cannot contain character except '-'.");
            }
            //size <= 0 then invalid
            if (size <= 0) System.out.println("Size must be positive.");
        }while(!isInteger || size <= 0);
        int[] array = new int[size];
        //init value for each element
        for (int i = 0; i < array.length;i++) {
            array[i] = new Random().nextInt(size);
        }
        bubbleSort sort = new bubbleSort();
        System.out.print("Unsorted array: ");
        sort.displayArray(array);
        sort.bubble(array);
        System.out.print("Sorted array: ");
        sort.displayArray(array);
    }
    
    
    public void displayArray(int[] array) {
        System.out.print("[");
        for (int i = 0; i < array.length-1; i++) {
            System.out.print(array[i]+", ");
        }
        System.out.println(array[array.length-1]+"]");
    }
    
    public void bubble(int[] array) {
        //range of sorting
        int range=array.length;
        //loop until range of swap process = 0
        while(range > 0) {
            //for each loop, range will decrease because max value is at the end of array
            range--;
            System.out.println(range);
            for (int i = 0; i < range; i++) {
                //if there is a number greater than its right number, swap them
                if(array[i]>array[i+1]) {
                    int temp = array[i];
                    array[i]=array[i+1];
                    array[i+1]=temp;
                }
            }
        }
    }
}
