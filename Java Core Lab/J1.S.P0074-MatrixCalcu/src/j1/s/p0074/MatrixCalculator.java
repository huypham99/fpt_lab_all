/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package j1.s.p0074;

import java.util.Scanner;

/**
 *
 * @author MSI
 */
public class MatrixCalculator {

    /**
     * @param args the command line arguments
     */
    static Scanner sc = new Scanner(System.in);
    public static void main(String[] args) {
        MatrixCalculator mc = new MatrixCalculator();
        int option;
        //loop until user choose exit
        do {
            option = mc.getMenuChoice();
            mc.Caculating(option);
            System.out.println("");
        }while(option>=1 && option <=3);
    }
    
    //display menu and get choice of user
    public int getMenuChoice() {
        System.out.println("=======Caculator Program=======");
        System.out.println("1. Addition Matrix");
        System.out.println("2. Subtraction Matrix");
        System.out.println("3. Multiplication Matrix");
        System.out.println("4. Quit");
        String optionStr;
        //repeat if user enter invalid choice
        do {
            System.out.print("Your choice: ");
            optionStr = sc.nextLine();
        }while(!checkOption(optionStr));
        return Integer.parseInt(optionStr);
    }
    //verify choice menu
    boolean checkOption(String optionStr) {
        int option;
        //check exception if user enter non-integer
        try {
            option = Integer.parseInt(optionStr);
        }catch(Exception e) {
            System.out.println("This cannot contain non-digit character");
            return false;
        }
        //if option is not in menu (out of range [1,4])
        if(option < 1 || option > 4) {
            System.out.println("Option "+option+" not in menu");
            return false;
        }
        return true;
    }
    
    public void Caculating(int option) {
        switch(option) {
            case 1:
                System.out.println("-------Addition-------");
                addition();
                break;
            case 2:
                System.out.println("-------Subtraction-------");
                subtraction();
                break;
            case 3:
                System.out.println("-------Multiplication-------");
                multiplication();
                break;
            case 4:
                System.out.println("Thanks for using!");
                break;
        }
    }
    
    public void multiplication() {
        int[][]matrix1,matrix2;
        matrix1 = setMatrixMulti(1,0);
        int col1 = matrix1[0].length;
        matrix2 = setMatrixMulti(2,col1);
        //display result
        System.out.println("--------- Result ----------");
        displayMatrix(matrix1);
        System.out.println("*");
        displayMatrix(matrix2);
        int rowResult = matrix1.length;
        int colResult = matrix2[1].length;
        //set row and collumn for matrix result [row1][col2]
        int[][] result = new int[rowResult][colResult];
        //calculating
        for (int i = 0; i < rowResult; i++) {
            for (int j = 0; j < colResult; j++) {
                for (int k = 0; k < col1; k++) {
                    result[i][j] += (matrix1[i][k]*matrix2[k][j]);
                }
            }
        }
        System.out.println("=");
        displayMatrix(result);
    }
    
    int[][] setMatrixMulti(int index, int col1) {
        int row,col;
        //with matrix 2
        if(index == 2) {
            //check row for matrix 2
            do {
                row = setSize("Row",2);
                if(row != col1) System.out.println("Row of matrix 2 must equal to"
                        + " column of matrix 1");
            }while(row != col1);
        //with matrix 1, set normally
        }else {
            row = setSize("Row",1);
        }
        col = setSize("Column",index);
        int[][]matrix = new int[row][col];
        setValue(matrix,index);
        return matrix;
    }
    
    public void subtraction() {
        int[][]matrix1,matrix2;
        matrix1 = setMatrixAddSub(1,0,0);
        int row1 = matrix1.length;
        int col1 = matrix1[0].length;
        matrix2 = setMatrixAddSub(2,row1,col1);
        
        System.out.println("--------- Result ----------");
        displayMatrix(matrix1);
        System.out.println("-");
        displayMatrix(matrix2);
        //calculating
        for (int i = 0; i < matrix1.length; i++) {
            for (int j = 0; j < matrix1[i].length; j++) {
                matrix1[i][j] -= matrix2[i][j];
            }
        }
        System.out.println("=");
        displayMatrix(matrix1);
    }
    
    public void addition() {
        int[][]matrix1,matrix2;
        matrix1 = setMatrixAddSub(1,0,0);
        int row1 = matrix1.length;
        int col1 = matrix1[0].length;
        matrix2 = setMatrixAddSub(2,row1,col1);
        System.out.println("--------- Result ----------");
        displayMatrix(matrix1);
        System.out.println("+");
        displayMatrix(matrix2);
        for (int i = 0; i < matrix1.length; i++) {
            for (int j = 0; j < matrix1[i].length; j++) {
                matrix1[i][j] += matrix2[i][j];
            }
        }
        System.out.println("=");
        displayMatrix(matrix1);
    }
    
    void displayMatrix(int[][]matrix) {
        for (int[] cols: matrix) {
            for (int element: cols) {
                System.out.print("["+element+"]");
            }
            System.out.println("");
        }
    }
    
    int[][] setMatrixAddSub(int index, int row1, int col1) {
        int row,col;
        //with matrix 2
        if(index == 2) {
            //check row for matrix 2
            do {
                row = setSize("Row",2);
                if(row != row1) System.out.println("Row of matrix 2 must equal to"
                        + " row of matrix 1");
            }while(row != row1);
            //check col for matrix 2
            do {
                col = setSize("Column",2);
                if(col != col1) System.out.println("Column of matrix 2 must equal to"
                        + " column of matrix 1");
            }while(col != col1);
        //with matrix 1, set normally
        }else {
            row = setSize("Row",1);
            col = setSize("Column",1);
        }
        int[][]matrix = new int[row][col];
        setValue(matrix,index);
        
        return matrix;
    }
    
    void setValue(int[][] matrix, int index) {
        int row = matrix.length;
        int col = matrix[1].length;
        //set value for matrix
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                matrix[i][j] = setElement(i,j,index);
            }
        }
    }
    
    int setElement(int i, int j, int index) {
        String element;
        do {
            System.out.print("Enter Matrix"+index+"["+(i+1)+"]["+(j+1)+"]: ");
            element = sc.nextLine();
        }while(!checkElement(element));
        return Integer.parseInt(element);
    }
    
    boolean checkElement(String element) {
        if(!checkBlank(element)) return false;
        if(!isInt(element)) {
            System.out.println("Values of matrix must be a number.");
            return false;
        }
        return true;
    }
    
    int setSize(String sizePrint, int index) {
        String size;
        do {
            System.out.print("Enter "+sizePrint+" Matrix "+index+": ");
            size = sc.nextLine();
        }while(!checkSize(size));
        return Integer.parseInt(size);
    }
    
    boolean checkSize(String sizeStr)  {
        if(!checkBlank(sizeStr)) return false;
        //if not a number
        if(!isInt(sizeStr)) {
            System.out.println("Please input a number.");
            return false;
        }
        //if size is 0 or negative
        if(Integer.parseInt(sizeStr)<=0) {
            System.out.println("Size of matrix must greater than 0.");
            return false;
        }
        return true;
    }
    
    boolean isInt(String string) {
        try {
            int temp = Integer.parseInt(string);
        }catch(NumberFormatException e) {
            return false;
        }
        return true;
    }
    
    boolean checkBlank(String string) {
        if(string.trim().isEmpty()) {
            System.out.println("This cannot blank.");
            return false;
        }
        return true;
    }
    
    boolean isDouble(String string) {
        try {
            double temp = Double.parseDouble(string);
        }catch(NumberFormatException e) {
            return false;
        }
        return true;
    }
}
