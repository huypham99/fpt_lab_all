
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Random;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author MSI
 */
public class BinarySearch {
    
    static Scanner sc = new Scanner(System.in);
    public static void main(String[] args) {
        int array[];
        int size;
        int searchVal;
        size = setSize();
        searchVal = setSearch(size);
        array = generateArray(size);
        //sort array by built-in function
        Arrays.sort(array);
        System.out.print("Sorted array: ");
        displayArray(array);
        //looking for index of search value in array by binary search
        int findIndex = binarySearch(array,0,size-1,searchVal,1);
        //if not found index of search value in array
        if(findIndex==-1) {
            System.out.println("Not found "+searchVal+" in array.");
        //if found
        }else System.out.println("Found "+searchVal+" at index: "+findIndex);
    }
    
    static int binarySearch(int array[], int start, int end, int searchVal, int step) {
        int middleI = findMedianIndex(array, start ,end);
        
        //if not middle not exist => not found search value
        if(middleI == -1) {
            System.out.println("Step "+step+" (searched value is absent)");
            return -1;
        }
        //if found search val
        if(searchVal == array[middleI]) {
            System.out.print("Step "+step+" (middle element is "
                    +array[middleI]+" == "+ searchVal+"): ");
            displayPartOfArray(array,start,end);
//            System.out.println("Index of middle: "+middleI);
            return middleI;
        }
        //if middle > search value, searching the left part side of middle
        else if(array[middleI] > searchVal) {
            System.out.print("Step "+step+" (middle element is "
                    +array[middleI]+" > "+ searchVal+"): ");
            displayPartOfArray(array,start,end);
//            System.out.println("Index of middle: "+middleI);
            return binarySearch(array,start,middleI-1,searchVal,++step);
        }
        //if middle > search value, searching the right part side of middle
        else if(array[middleI] < searchVal) {
            System.out.print("Step "+step+" (middle element is "
                    +array[middleI]+" < "+ searchVal+"): ");
            displayPartOfArray(array,start,end);
//            System.out.println("Index of middle: "+middleI);
            return binarySearch(array,middleI+1,end,searchVal,++step);
        }
        else return -1;
    }
    
    static void displayPartOfArray(int[] array, int start, int end) {
        //display part of array with start and end index
        for (int i = start; i <= end; i++) {
            System.out.print(array[i]+" ");
        }
        System.out.println("");
    }
    
    static int findMedianIndex(int array[], int begin, int end) {
        if(begin > end) return -1;
        return (int)((begin + end)/2);
    }
    
    static void displayArray(int[] array) {
        System.out.print("[");
        int size = array.length;
        for (int i = 0; i < size-1; i++) {
            System.out.print(array[i]+", ");
        }
        System.out.println(array[size-1]+"]");
    }
    
    static int[] generateArray(int size) {
        int[] array = new int[size];
        //generate random elements in array
        for (int i = 0; i < size; i++) {
            array[i] = Math.abs(new Random().nextInt())%(size+1);
        }
        return array;
    }
    
    static int setSearch(int size) {
        String searchVal;
        //validate search value
        do {
            System.out.println("Enter search value:");
            searchVal = sc.nextLine();
        }while(!checkSearch(searchVal,size));
        return Integer.parseInt(searchVal);
    }
    
    static boolean checkSearch(String searchStr, int size)  {
        if(!checkBlank(searchStr)) return false;
        //if not a number
        if(!isInt(searchStr)) {
            System.out.println("Please input a number.");
            return false;
        }
        int searchVal = Integer.parseInt(searchStr);
        //if searchVal is negative
        if(searchVal < 0) {
            System.out.println("Search value must greater than or equal to 0.");
            return false;
        }
        if(searchVal > size) {
            System.out.println("Search value must smaller than or equal to size.");
            return false;
        }
        return true;
    }
    
    static int setSize() {
        String size;
        //validate size of array
        do {
            System.out.println("Enter number of array:");
            size = sc.nextLine();
        }while(!checkSize(size));
        return Integer.parseInt(size);
    }
    
    static boolean checkSize(String sizeStr)  {
        if(!checkBlank(sizeStr)) return false;
        //if not a number
        if(!isInt(sizeStr)) {
            System.out.println("Please input a number.");
            return false;
        }
        //if searchVal is 0 or negative
        if(Integer.parseInt(sizeStr)<=0) {
            System.out.println("Size of matrix must greater than 0.");
            return false;
        }
        return true;
    }
    
    static boolean isInt(String string) {
        try {
            int temp = Integer.parseInt(string);
        }catch(NumberFormatException e) {
            return false;
        }
        return true;
    }
    
    static boolean checkBlank(String string) {
        if(string.trim().isEmpty()) {
            System.out.println("This cannot blank.");
            return false;
        }
        return true;
    }
}
