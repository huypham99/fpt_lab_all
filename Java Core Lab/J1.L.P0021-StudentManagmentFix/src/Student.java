/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author MSI
 */
public class Student{
    String id;
    String name;
    String course;
    String semester;

    public Student(String id, String name, String course, String semester) {
        this.id = id.trim();
        this.name = name.trim();
        String trimCourse = course.trim();
        this.course = (trimCourse.equalsIgnoreCase("java")?"Java"
                :(trimCourse.equalsIgnoreCase(".net")?".Net":"C/C++"));
        this.semester = semester.trim();
    }

    public Student() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name.trim();
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course.trim();
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester.trim();
    }

    
    
}
