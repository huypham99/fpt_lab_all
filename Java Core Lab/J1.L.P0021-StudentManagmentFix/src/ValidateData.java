/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author MSI
 */
public class ValidateData {
    static boolean checkBlank(String string) {
        int countSpace = 0;
        //count number of space in string
        for (int i = 0; i < string.length(); i++) {
            if(Character.isSpaceChar(string.charAt(i))) countSpace++;
        }
        //if the whole string is blank
        if(countSpace==string.length()) {
            System.out.println("This cannot blank.");
            return false;
        }
        return true;
    }
    
    static boolean isInt(String string) {
        try {
            int temp = Integer.parseInt(string);
        }catch(Exception e) {
            return false;
        }
        return true;
    }
    
    static boolean checkOption(String choiceStr) {
        if(!checkBlank(choiceStr)) return false;
        //check exception if user enter non-integer
        if(!isInt(choiceStr)) {
            System.out.println("This cannot contain non-digit character");
            return false;
        }
        int option=Integer.parseInt(choiceStr);
        //if option is not in menu (out of range [1,5])
        if(option < 1 || option > 5) {
            System.out.println("Option "+option+" not in menu");
            return false;
        }
        return true;
    }
    
    static boolean checkNoInList(String numberOrderStr, int sizeOfList) {
        if(!checkBlank(numberOrderStr)) return false;
        //if user not enter integer
        if(!isInt(numberOrderStr)) {
            System.out.println("This is not number order.");
            return false;
        }
        int numberOrder = Integer.parseInt(numberOrderStr);
        //if number order not in list
        if(numberOrder<1 || numberOrder > sizeOfList) {
            System.out.println("Record "+numberOrder+" not in list.");
            return false;
        }
        return true;
    }
    
    static boolean validateUpdate(String update) {
        if(!checkBlank(update)) return false;
        //if user not enter u or d ingnore case
        if(!(update.equalsIgnoreCase("u")||update.equalsIgnoreCase("d"))) {
            System.out.println("Must enter U or D.");
            return false;
        }
        return true;
    }
    
    static boolean validateExit(String exit) {
        if(!checkBlank(exit)) return false;
        //if user not enter y or n ignorecase
        if(!(exit.equalsIgnoreCase("y") || exit.equalsIgnoreCase("n"))) {
            System.out.println("Must enter Y or N.");
            return false;
        }
        return true;
    }
    
    static boolean checkPosInt(String posIntStr) {
        //check exception if user enter non-integer
        if(!isInt(posIntStr)) {
            System.out.println("This cannot contain non-digit character");
            return false;
        }
        int posInt=Integer.parseInt(posIntStr);
        if(posInt <= 0) {
            System.out.println("This must be greater than 0");
            return false;
        }
        return true;
    }
    
    static boolean checkID(String id) {
        if(!checkBlank(id)) return false;
        //ID must contain "FU" at the beginning and 3 digits end.
        if(!id.matches("FU[0-9]{3}")) {
            System.out.println("ID must in format FUxxx(x means a digit).");
            return false;
        }
        return true;
    }
    
    static boolean checkCourse(String course) {
        if(!checkBlank(course)) return false;
        //course must in (Java, .Net, C/C++)
        if(!(course.equalsIgnoreCase("java")||course.equalsIgnoreCase(".net")||
                course.equalsIgnoreCase("c/c++"))) {
            System.out.println("Course must be Java, .Net or C/C++.");
            return false;
        }
        return true;
    }
    
    static boolean checkSemester(String semester) {
        if(!checkBlank(semester)) return false;
        //semester must in (Spring, Summer, Fall)
        if(!(semester.equalsIgnoreCase("spring")||semester.equalsIgnoreCase("summer")||
                semester.equalsIgnoreCase("fall"))) {
            System.out.println("Semester must be Spring, Summer or Fall.");
            return false;
        }
        return true;
    }
}
