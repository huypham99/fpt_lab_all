
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author MSI
 */
public class StudentManagment {

    /**
     * @param args the command line arguments
     */
    static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        StudentList list = new StudentList();        
        int choice;
        //repeat until user want to exit
        do {
            choice = getUserChoice();
            //execute choice of user
            Execute(choice,list);
            System.out.println("");
        }while(choice != 5);
    }
    static void Execute(int choice, StudentList list) {
        switch(choice) {
            case 1:      
                create(list);                
                break;
            case 2:
                list.findAndSort();
                break;
            case 3:
                list.updateDel();
                break;
            case 4:
                list.report();
                break;
            case 5:
                System.out.println("Thanks for using!");
                break;
        }
    }
    
    static void create(StudentList list) {
        //repeat until size of list >= 3 then ask
        while(true) {
            list.create();
            System.out.println("\nCurrent number of record: "+list.size());
            if(list.size()>=3) {
                if(exitOrCont()) break;
            }
        }
    }
    
    static int getUserChoice() {
        String choice;
        //repeat until user enter valid choice
            do {
                displayMenu();
                choice = scanner.nextLine();
            }while(!ValidateData.checkOption(choice));
            return Integer.parseInt(choice);
    }
    
    
    static boolean exitOrCont() {
        String exit;
        do {
            System.out.println("Do you want to continue (Y/N)?");
            exit = scanner.nextLine();
        }while(!ValidateData.validateExit(exit));
        return exit.equalsIgnoreCase("n");
    }
    
    static void displayMenu() {
        System.out.println("=======WELCOME TO STUDENT MANAGEMENT=======");
        System.out.println("1. Create");
        System.out.println("2. Find and sort");
        System.out.println("3. Update/delete");
        System.out.println("4. Report");
        System.out.println("5. Exit");
        System.out.println("6. Display list");
        System.out.println("Your choice:");
    }
}
