
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author MSI
 */
public class StudentList extends ArrayList<Student>{
    static Scanner scanner = new Scanner(System.in);
    
    public void create() {
//        this.add(new Student("a","a","a","a"));
        
        String id, newName, newCourse, newSemester;
        int index;
        boolean exit;
        //repeat if user want to re-enter record
        do {
            exit=true;
            System.out.println("-----------New record-----------");  
            id = this.setNewId();
            index = this.findIndexByID(id);
            //if id not exist, enter new newName
            if (index==-1) {
                newName = this.setNewName();
            //else set newName = newName of existed id
            }else newName = this.get(index).getName();
            newCourse = this.setNewCourse();
            newSemester = this.setNewSemester();
            index = this.findIndexByRecord(id, newCourse, newSemester,this);
            //if record have existed
            if(index != -1) {
                System.out.println("This record have already existed.");
                //ask user want to back to menu or re-enter record
                exit = this.exitOrCont();
                if (exit) return;
            }
        }while(!exit);
        this.add(new Student(id,newName,newCourse,newSemester));
       
    }
    
    int findIndexByID(String ID) {
        for (int i = 0; i < this.size(); i++) {
            if(this.get(i).getId().equalsIgnoreCase(ID)) return i;
        }
        return -1;
    }
    
    public void findAndSort() {
        //if nothing in list
        if(this.isEmpty()) {
            System.out.println("The list is empty.");
            return;
        }
        String findName;
        //validate findName
        do {
            System.out.print("Enter name/part or name to find: ");
            findName = scanner.nextLine().trim();
        }while(!ValidateData.checkBlank(findName));
        ArrayList<Student> findList = new ArrayList<>();
        //add student have newName contain find newName to find list
        for (Student student: this) {
            if(student.getName().toLowerCase().contains(findName.toLowerCase()))
                findList.add(student);
        }
        //sort by newName
        Collections.sort(findList, new Comparator<Student>() {
            @Override
            public int compare(Student s1, Student s2) {
                return s1.getName().compareToIgnoreCase(s2.getName());
            }
        });
        this.displayList(findList);
    }
    
    public void updateDel() {
        //if nothing in list
        if(this.isEmpty()) {
            System.out.println("The list is empty.");
            return;
        }
        String findId = setFindId();
        //back to menu
        if(findId==null) return;
        ArrayList<Student> findIdList = new ArrayList<>();
        //add student have same id with findID to list
        for (Student student: this) {
            if(student.getId().equals(findId)) findIdList.add(student);
        }
        //display the list with find ID
        this.displayList(findIdList);
        int numberOrderFindList = setNo(findIdList.size());
        //index of record in the student list
        int index = getIndexInList(numberOrderFindList-1,findIdList);
        boolean update = this.UpdateOrDel();
        //if user want to update data
        if(update) {
            update(findId,index);
        }else {
            delete(index);
        }
    }
    
    void update(String findId, int index) {
        int changeIndex = index;
        Student changeStudent = this.get(index);
        String newId, newName, newCourse, newSemester;
        boolean exit;
        //repeat if user want to re-update
        do {
            exit=true;
            System.out.println("--------------------------------------------------");
            newId = this.setNewId();
            newName = this.setNewName();
            newCourse = this.setNewCourse();
            newSemester = this.setNewSemester();
            index = findIndexByRecord(newId, newCourse, newSemester, this);
            //if record have existed
            if(index != -1 && index != changeIndex) {
                System.out.println("This record have already existed.");
                exit = this.exitOrCont();
                if (exit) return;
            }
        }while(!exit);
        changeStudent.setId(newId);
        //set name for all records have newId
        for (Student student: this) {
            if (student.getId().equalsIgnoreCase(newId))
                student.setName(newName);
        }
        //set new newCourse for record
        changeStudent.setCourse(newCourse);
        //set new newSemester for record
        changeStudent.setSemester(newSemester);
        System.out.println("Update successfully!");
    }
    
    void delete(int index) {
        //delete record
        this.remove(index);
        System.out.println("Delete successfully.");
    }
    
    int getIndexInList(int indexFindList, List<Student> findIdList) {
        int index;
        String id=findIdList.get(indexFindList).getId();
        String course=findIdList.get(indexFindList).getCourse();
        String semester = findIdList.get(indexFindList).getSemester();
        index = this.findIndexByRecord(id, course, semester, this);
        return index;
    }
    
    boolean exitOrCont() {
        String exit;
        do {
            System.out.println("Do you want to continue (Y/N)?");
            exit = scanner.nextLine();
        }while(!ValidateData.validateExit(exit));
        return exit.equalsIgnoreCase("n");
    }
    
    boolean UpdateOrDel() {
        String update;
        do {
            System.out.println("Do you want to update(U) or delete(D) student?");
            update = scanner.nextLine();
        }while(!ValidateData.validateUpdate(update));
        return update.equalsIgnoreCase("u");
    }
    
    int setNo(int sizeOfList) {
        String numberOrder;
        do {
            System.out.print("Enter number order of record: ");
            numberOrder = scanner.nextLine();
        }while(!ValidateData.checkNoInList(numberOrder,sizeOfList));
        return Integer.parseInt(numberOrder);
    }
    
    public void displayList(List<Student> list) {
        //if nothing in list
        if(this.isEmpty()) {
            System.out.println("The list is empty.");
            return;
        }
        System.out.println("--------------------Display-----------------------");
        System.out.println("No.\tID\tName\t\tCourse\tSemester");
        int i=1;
        for(Student student: list) {
            System.out.println((i++)+"\t"+student.getId()+"\t"+student.getName()
                    +"\t\t"+student.getCourse()+"\t"+student.getSemester());
        }
        System.out.println("Number of records: "+list.size());
    }

    
    int findIndexByRecord(String id, String course, String semester, StudentList list) {
        for (int i = 0; i < list.size(); i++) {
            Student student = list.get(i);
            if(student.getId().equalsIgnoreCase(id)&&
                    student.getCourse().equalsIgnoreCase(course)&&
                    student.getSemester().equalsIgnoreCase(semester)) {
                return i;
            }
        }
        return -1;
    }
    
    public void report() {
        //if nothing in list
        if(this.isEmpty()) {
            System.out.println("The list is empty.");
            return;
        }
        //map to count number of newCourse for each set(id,newName,newCourse)
        Map <String,Integer> reportMap;
        reportMap = getReport(this);
        //list to sort report
        List <Entry<String, Integer>> reportList = new ArrayList<>(reportMap.entrySet());
        //sort report
        Collections.sort(reportList, new Comparator<Entry<String, Integer>>() {
            @Override
            public int compare(Entry<String, Integer> e1, Entry<String, Integer> e2) {
                return e1.getKey().compareToIgnoreCase(e2.getKey());
            }
        });
        System.out.println("--------------------------------------------------");
        System.out.println("No.\tID\tName\tCourse\t  NumberOfCourse");
        int i = 1;
        //print report after sort
        for (Entry<String, Integer> entry: reportList) {
            String idNameCourse = entry.getKey();
            int numberOfCourse = entry.getValue();
            System.out.println((i++)+"\t"+idNameCourse
                    +"\t\t"+numberOfCourse);
        }
    }
    
    Map getReport(StudentList list) {
        Map <String,Integer> report = new HashMap<>();
        for (Student student: list) {
            String key = student.getId()+"\t"+student.getName()+"\t"+student.getCourse();
            if(!report.containsKey(key)) {
                report.put(key, 1);
            }else {
                report.put(key, report.get(key)+1);
            }
        }
        return report;
    }
    
    
    String setNewId() {
        String id;
        //validate id
        do {
            System.out.print("Enter student ID: ");
            id = scanner.nextLine();
        }while(!ValidateData.checkBlank(id));
        return id.trim();
    }
    
    String setNewName() {
        String name;
        //validate newName
        do {
            System.out.print("Enter student name: ");
            name = scanner.nextLine();
        }while(!ValidateData.checkBlank(name));
        return name.trim();
    }
    
    String setNewCourse() {
        String course;
        //validate newCourse
        do {
            System.out.print("Enter course: ");
            course = scanner.nextLine();
        }while(!ValidateData.checkCourse(course));
        return course.trim();
    }
    
    String setNewSemester() {
        String semester;
        //validate newSemester
        do {
            System.out.print("Enter semester: ");
            semester = scanner.nextLine();
        }while(!ValidateData.checkBlank(semester));
        return semester.trim();
    }
    
    String setFindId() {
        String findId;
        boolean exit;
        //repeat when not found id and user choose continue
        do {
            exit = true;
            findId = setNewId();
            //if not found id
            if(this.findIndexByID(findId)==-1) {
                System.out.println("ID not found.");
                exit = this.exitOrCont();
                if(exit) return null;
            }
        }while(!exit);
        return findId.trim();
    }
}