
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author MSI
 */
public class CalculatorApp {
    static Scanner sc = new Scanner(System.in);
    
    public static void main(String[] args) {
        int choice;
        do {
            choice = getMenuChoice();
            switch(choice) {
                case 1:
                    System.out.println("\n----- Normal Calculator -----");
                    normal();
                    break;
                case 2:
                    System.out.println("\n----- BMI Calculator -----");
                    bmi();
                    break;
                case 3:
                    System.out.println("\nThanks for using!");
            }
            System.out.println("");
        }while(choice != 3);
    }
    
    static void normal() {
        double operand;
        String operator;
        double result;
        
        result = setOperand();
        //stop when operator equals "="
        while(true){
            operator = setOperator();
            if(operator.equals("=")) break;
            operand = setOperand();
            result = getResult(operator,operand,result);
            //display current memory
            System.out.println("Memory: "+result);
        }
        //display result
        System.out.println("Result: "+result);
    }
    
    static double getResult(String operator, double operand, double result) {
        //cases of operand
        switch(operator) {
            case "+":
                result+=operand;
                break;
            case "-":
                result-=operand;
                break;
            case "*":
                result*=operand;
                break;
            case "/":
                //exception divide by 0
                if(operand==0) {
                    System.out.println("Number cannot divide by 0.");
                    break;
                }
                result/=operand;
                break;
            case "^":
                result= Math.pow(result,operand);
                break;
        }
        return result;
    }
    
    static double setOperand() {
        String operand;
        //check format number
        do {
            System.out.print("Enter number: ");
            operand = sc.nextLine();
        }while(!ValidateData.checkOperand(operand));
        return Double.parseDouble(operand);
    }
    
    static String setOperator() {
        String operator;
        do {
            System.out.print("Enter operator: ");
            operator = sc.nextLine();
        }while(!ValidateData.checkOperator(operator));
        return operator;
    }
    
    static void bmi() {
        double weight = setWeight();
        double height = setHeight();
        double bmi = weight/(height*height);
        System.out.printf("BMI Number: %.2f\n",bmi);
        System.out.print("BMI Status: ");
        if (bmi < 19) System.out.println("UNDER-STANDARD");
        else if (bmi < 25) System.out.println("STANDARD");
        else if (bmi < 30) System.out.println("OVERWEIGHT");
        else if (bmi < 40) System.out.println("FAT - Should lose weight");
        else System.out.println("VERY FAT - Should lose weight immediately!");
    }
    
    static double setWeight() {
        String weight;
        do {
            System.out.print("Enter Weight(kg): ");
            weight = sc.nextLine();
        }while(!ValidateData.checkBMI(weight));
        return Double.parseDouble(weight);
    }
    
    static double setHeight() {
        String height;
        do {
            System.out.print("Enter Height(cm): ");
            height = sc.nextLine();
        }while(!ValidateData.checkBMI(height));
        return Double.parseDouble(height)/100;
    }
    
    static int getMenuChoice() {
        Scanner scanner = new Scanner(System.in);
        String choice;
        do {
                displayMenu();
                choice = scanner.nextLine();
            }while(!ValidateData.checkOption(choice));
        return Integer.parseInt(choice);
    }
    
    static void displayMenu() {
        System.out.println("======== Calculator Program ========");
        System.out.println("1. Normal Calculator");
        System.out.println("2. BMI Calculator");
        System.out.println("3. Exit");
        System.out.println("Please choose one option: ");
    }
    
}
