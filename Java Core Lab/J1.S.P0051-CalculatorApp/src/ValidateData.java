/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author MSI
 */
public class ValidateData {
    static boolean checkBlank(String string) {
        int countSpace = 0;
        //count number of space in string
        for (int i = 0; i < string.length(); i++) {
            if(Character.isSpaceChar(string.charAt(i))) countSpace++;
        }
        //if the whole string is blank
        if(countSpace==string.length()) {
            System.out.println("This cannot blank.");
            return false;
        }
        return true;
    }
    
    static boolean isDouble(String string) {
        try {
            double temp = Double.parseDouble(string);
        }catch(Exception e) {
            return false;
        }
        return true;
    }
    
    static boolean checkOperand(String operand) {
        if(!checkBlank(operand)) return false;
        //if not double
        if(!isDouble(operand)) {
            System.out.println("Please enter a number.");
            return false;
        }
        return true;
    }
    
    static boolean checkBMI(String statistic) {
        if(!checkBlank(statistic)) return false;
        //if not double
        if(!isDouble(statistic)) {
            System.out.println("BMI is digit.");
            return false;
        }
        if(Double.parseDouble(statistic)<=0) {
            System.out.println("BMI greater than 0.");
            return false;
        }
        return true;
    }
    
    static boolean checkOperator(String operator) {
        if(!checkBlank(operator)) return false;
        //if not (+,-,*,/,^)
        if(!operator.matches("[-+*/^=]")) {
            System.out.println("Please input (+, -, *, /, ^, =).");
            return false;
        }
        return true;
    }
    
    static boolean checkDivideByZero(double operand, String operator) {
        if(operand==0 && operator.equals("/")) {
            System.out.println("Number cannot divide by zero.");
            return false;
        }
        return true;
    }
    
    static boolean isInt(String string) {
        try {
            int temp = Integer.parseInt(string);
        }catch(Exception e) {
            return false;
        }
        return true;
    }
    
    static boolean checkOption(String choiceStr) {
        if(!checkBlank(choiceStr)) return false;
        //check exception if user enter non-integer
        if(!isInt(choiceStr)) {
            System.out.println("This cannot contain non-digit character");
            return false;
        }
        int option=Integer.parseInt(choiceStr);
        //if option is not in menu (out of range [1,5])
        if(option < 1 || option > 6) {
            System.out.println("Option "+option+" not in menu");
            return false;
        }
        return true;
    }
    
    
}
