/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package j1.s.p0007;

import java.util.Scanner;

/**
 *
 * @author MSI
 */
public class Main {
    static Scanner sc = new Scanner(System.in);
    public static void main(String[] args) {
        Main main = new Main();
//        //tc1
//        Object[] pointSet = {100,200,250,300,400};
//        //tc2
//        Object[] pointSet = {'a','b','c','d','e'};
        //tc3
        Object[] pointSet = {"HN","HD","HP","TP.HCM","NYC"};
        int vertexNumber = pointSet.length;
        boolean [][] adjacentMatrix = main.createGraph(vertexNumber);
//        main.addEdge(100,400,adjacentMatrix,pointSet);
//        main.addEdge(200,300,adjacentMatrix,pointSet);
//        main.addEdge(250,100,adjacentMatrix,pointSet);
//        main.addEdge(300,400,adjacentMatrix,pointSet);
//        main.addEdge(250,400,adjacentMatrix,pointSet);
//        main.addEdge(250,250,adjacentMatrix,pointSet);
//        //tc2
//        main.addEdge('a','c',adjacentMatrix,pointSet);
//        main.addEdge('b','d',adjacentMatrix,pointSet);
//        main.addEdge('a','d',adjacentMatrix,pointSet);
//        main.addEdge('e','c',adjacentMatrix,pointSet);
//        main.addEdge('e','e',adjacentMatrix,pointSet);
//        main.addEdge('b','c',adjacentMatrix,pointSet);
        //tc3
        main.addEdge("HN","HD",adjacentMatrix,pointSet);
        main.addEdge("TP.HCM","HP",adjacentMatrix,pointSet);
        main.addEdge("HN","TP.HCM",adjacentMatrix,pointSet);
        main.addEdge("HP","HD",adjacentMatrix,pointSet);
        main.addEdge("NYC","HN",adjacentMatrix,pointSet);
        main.addEdge("HN","HP",adjacentMatrix,pointSet);
        main.printAdjacencyMatrix(adjacentMatrix,pointSet);
        Object startToCheck, endToCheck;
        startToCheck = main.setPointToCheck(pointSet,"start");
        endToCheck = main.setPointToCheck(pointSet,"end");
        if(main.isEdge(startToCheck,endToCheck,adjacentMatrix,pointSet)) {
            System.out.println("This is an edge");
        }else System.out.println("This is not an edge");
    }
    
    public boolean isEdge(Object startPoint, Object endPoint, boolean[][] adjacentMatrix, Object[]pointSet){
        int startIndex = 0, endIndex = 0;
        //find corresponding index in adjacent matrix of start and end point 
        for (int i = 0; i < pointSet.length; i++) {
            if (pointSet[i].equals(startPoint)) startIndex = i;
            if (pointSet[i].equals(endPoint)) endIndex = i;
        }
        //return true if there is an edge, otherwise fall
        return adjacentMatrix[startIndex][endIndex];
    }
    
    public Object setPointToCheck(Object[] pointSet, String endOrStart) {
        Main main = new Main();
        Object checkPoint;
        //check whether startToCheck point is invalid
        do{
            System.out.println("Enter the "+endOrStart+" point:");
            checkPoint = sc.nextLine();
        }while(!main.isVertex(checkPoint.toString(),pointSet));
        //return integer if user enter number
        if(isInt(checkPoint.toString())) return Integer.parseInt(checkPoint.toString());
        //return double if user enter double
        else if(isDouble(checkPoint.toString())) return Double.parseDouble(checkPoint.toString());
        //return char if user enter 1 character
        else if(checkPoint.toString().length()==1) return checkPoint.toString().charAt(0);
        //return string if user enter > 1 character
        else return checkPoint.toString();
    }
    
    public boolean isVertex(String checkPoint, Object[] pointSet) {
        //check blank
        if(checkPoint.trim().isEmpty()) {
            System.out.println("This cannot empty.");
            return false;
        }
        //for each element in set of vertices
        for (int i = 0; i < pointSet.length; i++) {
            //if user enter integer number
            if(isInt(checkPoint)) {
                //if there is the same integer vertex with checkPoint in set of 
                //vertices,return true
                if(pointSet[i].equals(Integer.parseInt(checkPoint)))
                    return true;
            //if user enter double number
            }else if(isDouble(checkPoint)) {
                //if there is the same double vertex with checkPoint in set of 
                //vertices,return true
                if(pointSet[i].equals(Double.parseDouble(checkPoint)))
                    return true;
            //if user enter > 1 character    
            }else if(checkPoint.length()>1) {
                //if there is the same string vertex with checkPoint
                //in vertexSet, return true
                if(pointSet[i].equals(checkPoint))
                    return true;
            //if user enter 1 character,with each character in vertexSet
            }else if(pointSet[i].toString().length()==1){
                char tempCheckPoint = checkPoint.charAt(0);
                char tempPointInList = pointSet[i].toString().charAt(0);
                //if there is the same char in vertexSet, return true
                if(tempCheckPoint==tempPointInList) return true;
            }
        }
        //if there is not exist check point in vertex set, print
        System.out.println("This is not a vertex.");
        return false;
    }
    
    boolean isInt(String string) {
        try {
            int temp = Integer.parseInt(string);
        }catch(Exception e) {
            return false;
        }
        return true;
    }
    
    boolean isDouble(String string) {
        try {
            double temp = Double.parseDouble(string);
        }catch(Exception e) {
            return false;
        }
        return true;
    }
    
    public void printAdjacencyMatrix(boolean[][] adjacentMatrix, Object[] pointSet) {
        int vertexNum = pointSet.length;
        //display row of vertex set
        for (int i = 0; i < vertexNum; i++) {
            System.out.print(" \t"+pointSet[i]);
        }
        System.out.println("\n");
        //display adjacent matrix with column of vertex set
        for (int i = 0; i < vertexNum; i++) {
            System.out.print(pointSet[i]+"\t");
            for (int j = 0; j < vertexNum; j++) {
                //print 1 if true, 0 if false
                System.out.print((adjacentMatrix[i][j]?1:0)+"\t");
            }
            System.out.println("\n");
        }
    }
    
    boolean[][] createGraph(int vertexNumber) {
        boolean[][] adjacentMatrix=new boolean[vertexNumber][vertexNumber];
        for (int i = 0; i < vertexNumber;i++) {
            for (int j = 0; j < vertexNumber; j++) {
                adjacentMatrix[i][j]=false;
            }
        }
        return adjacentMatrix;
    }
    
    public void addEdge(Object startPoint, Object endPoint, boolean[][] adjacentMatrix, Object[]pointSet) {
        int startIndex = 0, endIndex = 0;
        for (int i = 0; i < pointSet.length; i++) {
            if (pointSet[i].equals(startPoint)) startIndex = i;
            if (pointSet[i].equals(endPoint)) endIndex = i;
        }
        adjacentMatrix[startIndex][endIndex] = true;
        adjacentMatrix[endIndex][startIndex] = true;
    }
}
