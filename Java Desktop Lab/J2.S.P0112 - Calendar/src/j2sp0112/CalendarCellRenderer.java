/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package j2sp0112;

import java.awt.Color;
import java.awt.Component;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 *
 * @author MSI
 */
public class CalendarCellRenderer extends DefaultTableCellRenderer {
    
    int year;
    String month;
    public CalendarCellRenderer(String month, int year) {
        this.month = month;
        this.year = year;
    }
    
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        SimpleDateFormat f = new SimpleDateFormat("dd-MMMM-yyyy");
        String[] today = f.format(new Date()).split("-");
        
        try {
            int date = Integer.valueOf(today[0]);
            int year = Integer.valueOf(today[2]);
            
            int day = Integer.parseInt(value.toString());
//            System.out.println("date: "+date + " - value: "+day);
//            System.out.println("month: "+today[1] + " - this month: "+this.month);
//            System.out.println("year: "+year + " - this year: "+year);
            if (day <= 31 && day > 7 && row == 0 || day >= 1 && day <= 11 && row >= 4) {
                c.setForeground(Color.GRAY);
            } else {
                c.setForeground(Color.BLACK);
            }
            c.setBackground(Color.WHITE);
            if(day == date && this.month.equals(today[1]) && this.year == year) {
                c.setBackground(Color.CYAN);
            }
        }catch(NumberFormatException e) {
            c.setBackground(Color.LIGHT_GRAY);
            c.setForeground(Color.BLACK);
        }
        return c;
    }

    @Override
    public void setHorizontalAlignment(int alignment) {
        super.setHorizontalAlignment(JLabel.CENTER);
    }
}
