/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package j2sp0112;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author MSI
 */
public class CalendarGBag extends JPanel{
    
    final int WIDTH,HEIGHT;
    
    JButton preBt;
    JButton nextBt;
    JLabel monthYearLabel;
    JTable calendarTable;
    DefaultTableModel tm;
    GridBagConstraints supporter;
    
    public CalendarGBag(JFrame container) {
        WIDTH = container.getWidth();
        HEIGHT = container.getHeight();
        setLayout(new GridBagLayout());
        supporter = new GridBagConstraints();
        
        preBt = new JButton();
        setImgButton(preBt, "src/j2sp0112/pre.png");
        preBt.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                setImgButton(preBt, "src/j2sp0112/pre.png");
            }
            @Override
            public void mousePressed(MouseEvent e) {
                setImgButton(preBt, "src/j2sp0112/prePress.png");
            }
        });
        
        supporter.gridx = 0;
        supporter.gridy = 0;
        supporter.gridwidth = 1;
        supporter.gridheight = 1;
        add(preBt,supporter);
        
        
        monthYearLabel = new JLabel("month year");
        monthYearLabel.setHorizontalAlignment(JLabel.CENTER);
        supporter.gridx = 1;
        supporter.gridy = 0;
        supporter.fill = GridBagConstraints.HORIZONTAL;
        supporter.insets = new Insets(0, 75, 0, 75);
        add(monthYearLabel,supporter);
        
        nextBt = new JButton();
        nextBt.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                setImgButton(nextBt, "src/j2sp0112/next.png");
            }
            @Override
            public void mousePressed(MouseEvent e) {
                setImgButton(nextBt, "src/j2sp0112/nextPress.png");
            }
        });
        
        supporter.gridx = 2;
        supporter.gridy = 0;
        setImgButton(nextBt, "src/j2sp0112/next.png");
        supporter.insets = new Insets(0, 0, 0, 0);
        supporter.fill = 0;
        add(nextBt,supporter);
        
        String columns[] = {"Su","Mo","Tu","We","Th","Fr","Sa"};
        calendarTable = new JTable(null,columns);
        tm = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false; //To change body of generated methods, choose Tools | Templates.
            }
        };
        calendarTable.setModel(tm);
        tm.setRowCount(6);
        supporter.gridx = 0;
        supporter.gridy = 1;
        supporter.gridwidth = 3;
        supporter.gridheight = 100;
        supporter.fill = GridBagConstraints.BOTH;
        JScrollPane s = new JScrollPane();
        add(new JScrollPane(calendarTable),supporter);
        
    }
    
    ImageIcon resizeIcon(ImageIcon icon, int resizedWidth, int resizedHeight) {
        Image img = icon.getImage();  
        Image resizedImage = img.getScaledInstance(resizedWidth, resizedHeight,  java.awt.Image.SCALE_SMOOTH);  
        return new ImageIcon(resizedImage);
    }
    
    void setImgButton(JButton button, String path) {
        button.setSize(20,20);
        button.setBorderPainted(false);
        button.setFocusable(false);
        button.setBorder(null);
        button.setMargin(new Insets(0, 0, 0, 0));
        button.setContentAreaFilled(false);
        ImageIcon pre = new ImageIcon(path);
        pre = resizeIcon(pre,button.getWidth()-5,button.getHeight()-5);
        button.setIcon(pre);
    }
    
    public static void main(String[] args) {
//         /* Set the Nimbus look and feel */
//        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
//        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
//         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
//         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(CalendarGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(CalendarGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(CalendarGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(CalendarGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
//        //</editor-fold>
        JFrame mainFrame = new JFrame();
        mainFrame.setSize(300, 300);
        mainFrame.add(new CalendarGBag(mainFrame));
        
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainFrame.setResizable(false);
        mainFrame.setVisible(true);
    }
}
