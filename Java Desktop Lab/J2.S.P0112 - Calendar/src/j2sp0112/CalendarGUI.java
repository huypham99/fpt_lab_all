/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package j2sp0112;

import java.awt.Font;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.LayoutStyle;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;

/**
 *
 * @author MSI
 */
public class CalendarGUI extends javax.swing.JFrame {

    
    DefaultTableModel tm;
    Calendar cal;
    CalendarCellRenderer render;
    
    public CalendarGUI() {
        initComponents();
        setTitle("Calendar");
        cal = new GregorianCalendar();
        
        monthYearLabel.setHorizontalAlignment(SwingConstants.CENTER);
        monthYearLabel.setFont(new Font("Arial", Font.PLAIN, 16));
        
        setImgButton(preBt,"img/pre.png");
        setImgButton(nextBt,"img/next.png");
        
        setTable();
        
        setResizable(false);
        setLocationRelativeTo(this);
        //update current month to calendar table
        updateMonth();
        
    }
    
    void setTable() {
        calendarTable.setFont(new Font("Arial",Font.PLAIN,14));
        tm = new DefaultTableModel(){
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        calendarTable.setModel(tm);
        
        String columns[] = {"Su","Mo","Tu","We","Th","Fr","Sa"};
        tm.setColumnIdentifiers(columns);
        
        calendarTable.setBorder(null);
        //enable select a cell
        calendarTable.setCellSelectionEnabled(true);
        //disable select row and column
        calendarTable.setRowSelectionAllowed(false);
        calendarTable.setColumnSelectionAllowed(false);
        calendarTable.setRowHeight(25);
        
        tm.setRowCount(6);
        
    }
    
    void updateMonth() {
        //set current day of month to 1st
        cal.set(Calendar.DAY_OF_MONTH, 1);
        //get current month
        String month = cal.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.US);
        //get current year
        int year = cal.get(Calendar.YEAR);
        monthYearLabel.setText(month+" "+year);
        //get the weekday of day 1st in current month
        int weekday = cal.get(Calendar.DAY_OF_WEEK);
        //get max number of days
        int daysOfMonth = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
        //decrese weekday to the index (sun == 1 --> 0, mon == 2 --> 1...)
        weekday--;
        //get index of day 1st
        int startDay = weekday;
        //set day from 1 to max days of month to right position
        for (int day = 1; day <= daysOfMonth; day++) {
            tm.setValueAt(day, weekday/7, weekday%7);
            weekday++;
        }
        //number of after remaining cell
        int n = 42-weekday;
        //set days of next month from 1 to n
        for (int i = 1; i <= n; i++) {
            tm.setValueAt(i, weekday/7, weekday%7);
            weekday++;
        }
        //set calendar to last day of pre-month
        cal.add(Calendar.DAY_OF_MONTH,-1);
        //max day of previous month
        int daysOfPreMonth = cal.get(Calendar.DAY_OF_MONTH);
        //from days of pre-month to right position
        for (int i = startDay-1; i >= 0; i--) {
            tm.setValueAt(daysOfPreMonth, 0, i);
            daysOfPreMonth--;
        }
        //set month back to current
        cal.add(Calendar.MONTH,1);
        //object to edit each cell of table
        render = new CalendarCellRenderer(month,year);
        calendarTable.setDefaultRenderer(Object.class, render);
        JTableHeader header = calendarTable.getTableHeader();
        header.setDefaultRenderer(render);
    }
    
    ImageIcon resizeIcon(ImageIcon icon, int resizedWidth, int resizedHeight) {
        Image img = icon.getImage();  
        //set image to resized size and smoothly
        Image resizedImage = img.getScaledInstance(resizedWidth, resizedHeight,  java.awt.Image.SCALE_SMOOTH);  
        return new ImageIcon(resizedImage);
    }
    
    void setImgButton(JButton button, String path) {
//        button.setBorderPainted(false);
        button.setBorder(null);
        button.setFocusable(false);
        button.setMargin(new Insets(0, 0, 0, 0));
        button.setContentAreaFilled(false);
        ImageIcon pre = new ImageIcon(getClass().getClassLoader().getResource(path));
        pre = resizeIcon(pre,button.getWidth()-5,button.getHeight()-5);
        button.setIcon(pre);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        preBt = new JButton();
        monthYearLabel = new JLabel();
        nextBt = new JButton();
        jScrollPane1 = new JScrollPane();
        calendarTable = new JTable();

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        preBt.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent evt) {
                preBtMousePressed(evt);
            }
            public void mouseReleased(MouseEvent evt) {
                preBtMouseReleased(evt);
            }
        });
        preBt.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                preBtActionPerformed(evt);
            }
        });

        monthYearLabel.setText("Month");

        nextBt.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent evt) {
                nextBtMousePressed(evt);
            }
            public void mouseReleased(MouseEvent evt) {
                nextBtMouseReleased(evt);
            }
        });
        nextBt.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                nextBtActionPerformed(evt);
            }
        });

        calendarTable.setModel(new DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(calendarTable);

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addComponent(preBt, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(monthYearLabel, GroupLayout.PREFERRED_SIZE, 216, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(nextBt, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, GroupLayout.PREFERRED_SIZE, 351, GroupLayout.PREFERRED_SIZE))
        );
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(33, 33, 33)
                        .addComponent(monthYearLabel))
                    .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addComponent(preBt, GroupLayout.Alignment.TRAILING, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE)
                            .addComponent(nextBt, GroupLayout.Alignment.TRAILING, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE))))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, GroupLayout.DEFAULT_SIZE, 186, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void preBtMousePressed(MouseEvent evt) {//GEN-FIRST:event_preBtMousePressed
        setImgButton(preBt, "img/prePress.png");
    }//GEN-LAST:event_preBtMousePressed

    private void preBtMouseReleased(MouseEvent evt) {//GEN-FIRST:event_preBtMouseReleased
        setImgButton(preBt, "img/pre.png");
    }//GEN-LAST:event_preBtMouseReleased

    private void nextBtMousePressed(MouseEvent evt) {//GEN-FIRST:event_nextBtMousePressed
        setImgButton(nextBt, "img/nextPress.png");
    }//GEN-LAST:event_nextBtMousePressed

    private void nextBtMouseReleased(MouseEvent evt) {//GEN-FIRST:event_nextBtMouseReleased
        setImgButton(nextBt, "img/next.png");
    }//GEN-LAST:event_nextBtMouseReleased

    private void preBtActionPerformed(ActionEvent evt) {//GEN-FIRST:event_preBtActionPerformed
        cal.add(Calendar.MONTH, -1);
        updateMonth();
    }//GEN-LAST:event_preBtActionPerformed

    private void nextBtActionPerformed(ActionEvent evt) {//GEN-FIRST:event_nextBtActionPerformed
        cal.add(Calendar.MONTH, 1);
        updateMonth();
    }//GEN-LAST:event_nextBtActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(CalendarGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(CalendarGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(CalendarGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(CalendarGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new CalendarGUI().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private JTable calendarTable;
    private JScrollPane jScrollPane1;
    private JLabel monthYearLabel;
    private JButton nextBt;
    private JButton preBt;
    // End of variables declaration//GEN-END:variables
}
