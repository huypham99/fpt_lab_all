/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package j2sp0001;

import java.awt.Image;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.ImageIcon;

/**
 *
 * @author MSI
 */
public class ImageTool {
    
    ImageIcon resizedIcon(ImageIcon icon, int resizedWidth, int resizedHeight) {
        Image img = icon.getImage();
        //resize image with resized width and height
        img = img.getScaledInstance(resizedWidth, resizedHeight, Image.SCALE_FAST);
        return new ImageIcon(img);
    }
    
    public void setScaledButtonImgIcon(JButton button,String path) {
        //construct an image icon from path
        ImageIcon myIcon = new ImageIcon(getClass().getClassLoader().getResource(path));
        //create new icon by scaled image
        myIcon = resizedIcon(myIcon, button.getWidth() - 10, button.getHeight() - 10);
        button.setIcon(myIcon);
    }
    
    public void setScaledLabelImgIcon(JLabel label,String path) {
        //construct an image icon from path
        ImageIcon myIcon = new ImageIcon(getClass().getClassLoader().getResource(path));
        myIcon = resizedIcon(myIcon, label.getWidth(), label.getHeight());
        label.setIcon(myIcon);
    }
}
