#include<stdio.h>
#include<string.h>
#include<ctype.h>
#include<stdlib.h>
#include<math.h>

int checkPositiveDouble(double posDouble, char check) {
    //return 0 if user enter non-digit character
    if(check != '\n') {
        printf("This must be a double number. Please re-enter.\n");
        return 0;
    }
    //return 0 if user enter negative number
    if(posDouble <= 0) {
        printf("This must be greater than 0.\n");
        return 0;
    }
        return 1;
}

void input(double &inputAmountOwed, double &annualInterest, double &inputPayment) {
    //check input
    char check;
    //repeat if input is invalid
    do {
        printf("What is the value left on the mortgage?\n");
        scanf("%lf",&inputAmountOwed);
        check = getchar();
        fpurge(stdin);
    }while(!checkPositiveDouble(inputAmountOwed,check));
    //repeat if input is invalid
    do {
        printf("What is the annual interest rate of the loan, in percent?\n");
        scanf("%lf",&annualInterest);
        check = getchar();
        fpurge(stdin);
    }while(!checkPositiveDouble(annualInterest,check));
    //repeat if input is invalid
    do {
        printf("What is the monthly payment?\n");
        scanf("%lf",&inputPayment);
        check = getchar();
        fpurge(stdin);
    }while(!checkPositiveDouble(inputPayment,check));
    printf("\n");
}

int main() {
    printf("------------Payment and Total Expenditure-------------\n");
    double inputAmountOwed;
    double interest;
    double inputPayment;
    input(inputAmountOwed,interest,inputPayment);
    //calculate monthly interest (not in percent)
    interest = interest/(100*12);
    //array to contain monthly payment
    double *payment = (double*)malloc(50*sizeof(double));
    //array to contain monthly amount owed
    double *amountOwed = (double*)malloc(50*sizeof(double));
    //initialize amount of payment at first
    payment[0] = inputPayment;
    //initialize amount owed at first
    amountOwed[0] = inputAmountOwed;
    //first month
    int month = 1;
    //print out heading of the chart
    puts("Month\t\tPayment\t\t\tAmount Owed");
    //stop when last amountOwed = 0
    while(amountOwed[month-1] > 0) {
        //if payment last month is greater than amount owed last month, then assign this month payment = last month amount owed
        if(payment[month-1] > amountOwed[month-1]*(1+interest)) {
            payment[month] = amountOwed[month-1]*(1+interest);
        }
        //else un-change payment
        else {
            payment[month] = payment[month-1];
        }
        //calculate amount owed this month: amount owed last month - payment this month  
        amountOwed[month] = amountOwed[month-1]*(1+interest) - payment[month];
        //print out line of the chart in current month
        printf("%d\t\t%.2lf\t\t\t%0.2lf\n",month,payment[month],amountOwed[month]);
        month++;
    }
    
}