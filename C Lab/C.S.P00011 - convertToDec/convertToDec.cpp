#include<stdio.h>
#include<string.h>
#include<ctype.h>
#include<stdlib.h>
#include<math.h>
#include<windows.h>
#include <bits/regex.h>

int checkBlank(char *string) {
    int countBlank=0;
    //counting space char in string
    for (int i = 0; i < strlen(string);i++) {
        if(string[i]==' ') countBlank++;
    }
    //if string is blank, return 0
    if(countBlank==strlen(string)) return 0;
    return 1;
}

int checkBin(char *stringBin) {
    //if input is blank
    if(!checkBlank(stringBin)) {
        printf("Your input cannot blank.\n");
        return 0;
    }
    //if input contain non-digit character
    for (int i = 0; i < strlen(stringBin);i++) {
        if(stringBin[i] != '0' && stringBin[i] != '1' ) {
            printf("Binary string must contain only 0 and 1.\n");
            return 0;
        }
    }
    //check range of convert decimal, cannot over limit of integer(2,147,483,647)
    if(strlen(stringBin)>31) {
        printf("Convert decimal is over the limit of integer (2,147,483,647)\n");
        return 0;
    }
    return 1;
}

void convertToDec(char *stringBase, int &dec, int base) {
    int power=strlen(stringBase)-1;
    dec = 0;
    int digitVal;
    for (int i = 0; stringBase[i]!='\0'; i++) {
        if (stringBase[i]>='0'&&stringBase[i]<='9') {
            digitVal = stringBase[i]-'0';
        }
        if (stringBase[i] >= 'A' && stringBase[i] <= 'F') {
            digitVal = stringBase[i] - 55;
        }
        dec+=pow(base,power)*digitVal;
        power--;
    }
}

void bin() {
    char *stringBin = (char*)malloc(100*sizeof(char));
    int dec;
    do {
        printf("Please enter your binary number: ");
        gets(stringBin);
        fpurge(stdin);
    }while(!checkBin(stringBin));
    stringBin = (char*)realloc(stringBin,strlen(stringBin)*sizeof(char));
    convertToDec(stringBin,dec,2);
    printf("decimal number = %d",dec);
}

int checkOc(char *stringOc) {
    //if input is blank
    if(!checkBlank(stringOc)) {
        printf("Your input cannot blank.\n");
        return 0;
    }
    //if input contain non-digit character
    for (int i = 0; i < strlen(stringOc);i++) {
        if(!isdigit(stringOc[i])) {
            printf("Octal number cannot contain non-digit character.\n");
            return 0;
        }
    }
    //if there is a digit over 7
    for (int i = 0; i < strlen(stringOc);i++) {
        if(stringOc[i]>'7') {
            printf("Octal digit must be in range [0,7].\n");
            return 0;
        }
    }
    //check range of convert decimal, cannot over limit of integer(2,147,483,647)
    if(strlen(stringOc)>11 || strlen(stringOc)==11 && stringOc[0]>'1') {
        printf("Convert decimal is over the limit of integer (2,147,483,647)\n");
        return 0;
    }
    return 1;
}

void octal() {
    char *stringOc = (char*)malloc(100*sizeof(char));
    int dec;
    do {
        printf("Please enter your octal number: ");
        gets(stringOc);
        fpurge(stdin);
    }while(!checkOc(stringOc));
    stringOc = (char*)realloc(stringOc,strlen(stringOc)*sizeof(char));
    convertToDec(stringOc,dec,8);
    printf("decimal number = %d",dec);
}

int checkHex(char *stringHex) {
    //if input is blank
    if(!checkBlank(stringHex)) {
        printf("Your input cannot blank.\n");
        return 0;
    }
    //if input contain special character
    for (int i = 0; i < strlen(stringHex); i++) {
        if(!(isalpha(stringHex[i]) || isdigit(stringHex[i]))) {
            printf("Input cannot contain special character.\n");
            return 0;
        }
    }
    //if input is lowercase
    for (int i = 0; i < strlen(stringHex); i++) {
        if(stringHex[i] >= 'a' && stringHex[i] <= 'z') {
            printf("Please uppercase all letter.\n");
            return 0;
        }
    }
    //if input not in range [0-9] [A-F]
    for (int i = 0; i < strlen(stringHex); i++) {
        if(!(isdigit(stringHex) || stringHex[i] >= 'A' && stringHex[i] <= 'F')) {
            printf("Input must be only in range [0-9] or [A-F].\n");
            return 0;
        }
    }
    //check range of convert decimal, cannot over limit of integer(2,147,483,647)
    if (strlen(stringHex)>8 || strlen(stringHex)==8 && stringHex[0]>'7') {
        printf("Convert decimal is over the limit of integer (2,147,483,647)\n");
        return 0;
    }
    return 1;
}

void hex() {
    char *stringHex = (char*)malloc(1000*sizeof(char));
    int dec;
    do {
        printf("Please enter your hex number: ");
        gets(stringHex);
        fpurge(stdin);
    }while(!checkHex(stringHex));
    stringHex = (char*)realloc(stringHex,strlen(stringHex)*sizeof(char));
    convertToDec(stringHex,dec,16);
    printf("decimal number = %d",dec);
}

int main() {
    int choice;
    printf("1. bin --> decimal\n");
    printf("2. octal --> decimal\n");
    printf("3. hexadecimal --> decimal\n");
    printf("Please enter choice:\n");
    scanf("%d",&choice);
    fpurge(stdin);
    switch(choice) {
        case 1:
            bin();
            break;
        case 2:
            octal();
            break;
        case 3:
            hex();
            break;
    }
}