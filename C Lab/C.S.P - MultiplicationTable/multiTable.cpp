#include <cstdlib>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

int checkInput(int inputNum, char check) {
    //if check is character not '\n' => input is not a number, return 0
    if (check != '\n') {
        printf("Data type error.\n");
        return 0;
    }
    //if input number out of required range, return 0
    if (inputNum < 1 || inputNum > 9) {
        printf("Input number must be in range(0,9).\n");
        return 0; 
    }
    //else return 1
    return 1;
}

void input(int &inputNum) {
    //char to check input
    char check;
    //loop if check invalid
    do {
        printf("Please enter a number (1-9): ");
        scanf("%d",&inputNum);
        check = getchar();
        //clear buffer
        fpurge(stdin);
    }while(checkInput(inputNum,check)==0);
}

void printMultiplicationTable(int inputNum) {
    printf("Multiplication table for %d number:\n",inputNum);
    //print multiplication table
    for (int i = 1; i <= 10; i++) {
        printf("%d x %d = %d\n",inputNum,i,inputNum*i);
    }
}

int checkExit(char *exit) {
    //if user enter != 'yes' and 'no', return 0.
    if (strcmp(exit,"yes")!=0 && strcmp(exit,"no")!=0) {
        printf("You must enter 'yes' or 'no'.\n");
        return 0;
    }
    return 1;
}

void multiplicationTable() {
    int inputNum;
    char *exit = (char*)malloc(100*sizeof(char));
    //repeat if user want to continue
    do {
        input(inputNum);
        printMultiplicationTable(inputNum);
        //repeat if user enter != 'yes' and 'no'
        do {
            printf("Enter 'yes' to continue,'no' to exit.\n");
            gets(exit);
            exit = (char*)realloc(exit,strlen(exit)*sizeof(char));
            fpurge(stdin);
        }while(checkExit(exit)==0);
        //Thank player when program end
        if (strcmp(exit,"no")==0) {
            printf("Thanks for using.");
        }
    }while(strcmp(exit,"yes")==0);
}


int main() {
    printf("Program to print multiplication table for input number.");
    printf("\n--------------------------------\n");
    multiplicationTable();
}