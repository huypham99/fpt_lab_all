#include<stdio.h>
#include<string.h>
#include<ctype.h>
#include<stdlib.h>
#include<math.h>

bool checkRangeOfInt(char *decString) {
    return !(strcmp(decString,"2147483647")>0);
}

int checkDec(char *decString) {
    //if first char is not number or '-', return 0
    if(!(isdigit(decString[0]) || decString[0]=='-')) {
        printf("Decimal cannot contain letter or special character. Please re-enter.\n");
        return 0;
    }
    //for each char from second in string
    for (int i = 1; i < strlen(decString);i++) {
        //if there is a non-digit char, return 0
        if(!(isdigit(decString[i]))) {
            printf("Decimal cannot contain letter or special character. Please re-enter.\n");
            return 0;
        }
    }
    //in this program, negative decimal is not accepted
    if (decString[0]=='-') {
        printf("In this program, input must be greater than or equal to 0.Please re-enter.\n");
        return 0;
    }
    if(strlen(decString)>=11 || (strlen(decString)==10 && !checkRangeOfInt(decString))) {
        printf("Integer cannot greater than 2,147,483,647.\n");
        return 0;
    }
    return 1;
}

void convertDec(int &dec, char *decString) {
    dec = 0;
    int power = strlen(decString)-1;
    for (int i = 0; i < strlen(decString);i++) {
        dec += pow(10,power)*(decString[i]-'0');
        power--;
    }
}

void inputDec(int &dec) {
    //to check input
    char check;
    //string of decimal
    char *decString = (char*)malloc(50*sizeof(char));
    //repeat if input is invalid
    do {
        printf("\tEnter a positive number: ");
        gets(decString);
        fpurge(stdin);
    }while(!checkDec(decString));
    decString = (char*)realloc(decString,strlen(decString)*sizeof(char));
    dec = atoi(decString);
}

void swap(int &firstNum, int &secondNum) {
    int temp = firstNum; firstNum = secondNum; secondNum = temp;
}

void reverseArray(int *array, int size) {
    int i = 0;
    int j = size - 1;
    //loop until i > j
    while(i < j) {
        swap(array[i],array[j]);
        i++;
        j--;
    }
}

void outputBin(int dec) {
    //converted bin array
    int *bin = (int*)malloc(100*sizeof(int));
    //size of bin array
    int size=0;
    //loop until dec = 0
    while(dec > 0) {
        //re-allocate
        bin = (int*)realloc(bin,(size+1)*sizeof(int));
        bin[size] = dec%2;
        dec/=2;
        size++;
    }
    reverseArray(bin,size);
    printf("\tBinary number: ");
    //print out binary string
    int i;
    for (i = 0; i < size; i++) {
        printf("%d",bin[i]);
    }
    printf("\n%d\n",i);
}

int checkCont(char cont, char check) {
    //if user enter more than 2 character
    if (check != '\n') {
        printf("You cannot enter more than 1 character. Please re-enter.\n");
        return 0;
    }
    // if user enter 1 character but it != Y and != N
    if (cont != 'Y' && cont != 'N') {
        printf("Please re-enter 'Y' or 'N'!\n");
        return 0;
    }
    return 1;
}

void ContOrExit(char &cont) {
    //to check input
    char check;
    //repeat until user enter valid input
    do {
        printf("Do you want to do another conversion (Y/N): ");
        cont = getchar();
        check = getchar();
        fpurge(stdin);
    }while(!checkCont(cont,check));
    //if user want to exit, thanks
    if(cont == 'N') printf("Thanks for using!");
}

int main() {
    printf("Convert Decimal to Binary program\n");
    //continue or not
    char cont;
    //input decimal
    int dec;
    //repeat until user want to exit
    do {
        inputDec(dec);
        outputBin(dec);
        ContOrExit(cont);
    }while(cont == 'Y');
}