#include<stdio.h>
#include<string.h>
#include<ctype.h>
#include<stdlib.h>
#include<math.h>

int checkOption(int option, char check) {
    //return false if user enter non-digit character
    if(check != '\n') {
        printf("Option must be an integer. Please re-enter.\n");
        return 0;
    }
    //return false if input option not in menu
    if(option < 1 || option > 3) {
        printf("%d is not an option in menu. Please re-enter.\n",option);
        return 0;
    }
        return 1;
}

int checkDouble(double coef, char check) {
    //if user enter a non-double number
    if (check != '\n') {
        printf("Input cannot contain letter or special character. Please re-enter.\n");
        return 0;
    }
    return 1;
}

int checkPositiveDouble(double posDouble, char check) {
    //if user enter a non-double number
    if (!checkDouble(posDouble,check)) return 0;
    //if user enter negative number
    if(posDouble <= 0) {
        printf("Input must be a positive number. Please re-enter.\n");
        return 0;
    }
    return 1;
}

int checkInterest(double interest, char check) {
    //if user enter a non-positive double number
    if (!checkPositiveDouble(interest,check)) return 0;
    //if user enter negative number
    if(interest > 0.1) {
        printf("Input cannot be greater than 0.1! Please re-enter.\n");
        return 0;
    }
    return 1;
}

int checkPositiveInt(int posInteger, char check) {
    //if user enter a non-digit character
    if (check != '\n') {
        printf("Input must be a number. Please re-enter.\n");
        return 0;
    }
    //if user enter negative number
    if(posInteger <= 0) {
        printf("Input must be larger than 0. Please re-enter.\n");
        return 0;
    }
    return 1;
}

void displayMenu(int &option) {
    //to check input
    char check;
    //repeat if input option invalid
    do{
        printf("MENU\n-------------------------------------------");
        printf("\n1- Quadratic equation");
        printf("\n2- Bank deposit problem");
        printf("\n3- Quit");
        printf("\nEnter Your Choice: ");
        scanf("%d",&option);
        check = getchar();
        fpurge(stdin);
    }while(!checkOption(option,check));
}

void enterCoef(double &a, double &b, double &c) {
    //to check input
    char check;
    //repeat if input a is invalid
    do {
        printf("Enter coefficient a: ");
        scanf("%lf",&a);
        check = getchar();
        fpurge(stdin);
    }while(!checkDouble(a,check));
    //repeat if input b is invalid
    do {
        printf("Enter coefficient b: ");
        scanf("%lf",&b);
        check = getchar();
        fpurge(stdin);
    }while(!checkDouble(b,check));
    //repeat if input c is invalid
    do {
        printf("Enter coefficient c: ");
        scanf("%lf",&c);
        check = getchar();
        fpurge(stdin);
    }while(!checkDouble(c,check));
}

void quadratic() {
    //coefficient of equation
    double a,b,c;
    //delta of quadratic equation
    double delta;
    printf("Quadratic equation: ax^2 + bx + c = 0\n");
    enterCoef(a,b,c);
    //if a == 0 => linear equation
    if(a==0) {
        //if b = 0 and c != 0, no solution
        if(b==0 && c != 0) printf("NO SOLUTION.\n");
        //if both b and c = 0, equation has infinite solutions
        if(b==0 && c==0) printf("INFINITE SOLUTION\n");
        //if b != 0, equation have 1 root x = -c/b
        if(b!=0) printf("\tx = %.2lf\n",-(double)c/b);
    //a != 0 => quadratic equation
    }else {
        delta = b*b - 4*a*c;
        //if delta < 0 => no solution
        if(delta < 0) printf("NO SOLUTION.\n");
        //if delta = 0 => 1 root
        if(delta==0) printf("\tx = %lf\n",-b/(2.0*a));
        //if delta > 0 => 2 distinct roots
        if(delta > 0) {
            printf("2 distinct roots:\n");
            printf("\tx1 = %.2lf\n",(-b - sqrt(delta))/(2.0*a));
            printf("\tx2 = %.2lf\n",(-b + sqrt(delta))/(2.0*a));
        }
    }
}

void deposit() {
    //to check input
    char check;
    //deposit money
    double deposit;
    //interest rate
    double interest;
    //number of months
    int month;
    //repeat if input deposit invalid
    do {
        printf("Enter the amount of deposit($): ");
        scanf("%lf",&deposit);
        check = getchar();
        fpurge(stdin);
    }while(!checkPositiveDouble(deposit,check));
    //repeat if input interest invalid
    do {
        printf("Enter deposit interest rate: ");
        scanf("%lf",&interest);
        check = getchar();
        fpurge(stdin);
    }while(!checkInterest(interest,check));
    //repeat if input month invalid
    do {
        printf("Enter the number of deposit months: ");
        scanf("%d",&month);
        check = getchar();
        fpurge(stdin);
    }while(!checkPositiveInt(month,check));
    printf("Your total amount received: $%.2lf\n",deposit*(1+interest*month));
}

int main() {
    printf("Program to use simple menu to manage program functions\n");
    //option of menu
    int option;
    //repeat while option == 1 or == 2
    do {
        displayMenu(option);
        //option will be process
        switch(option) {
            case 1:
                quadratic();
                break;  
            case 2:
                deposit();
                break;
            default:
                printf("Thanks for using!");
        }
    }while(option != 3);
}