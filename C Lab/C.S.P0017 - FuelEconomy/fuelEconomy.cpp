  #include<stdio.h>
#include<string.h>
#include<ctype.h>
#include<stdlib.h>
#include<math.h>
//pi
const double PI = 3.14159265358979323846264338327950288419716939937510582097494;
//to check input
char check;

int checkChoice(int option, char check) {
    //input choice contain letter or special character
    if(check != '\n') {
        printf("Option must be an integer number. Please re-enter.\n");
        return 0;
    }
    if(option < 1 || option > 4) {
        printf("Your choice is not in menu. Please re-enter.\n");
        return 0;
    }
        return 1;
}

void displayMenu(int &option) {
    //to check input
    char check;
    do {
        printf("1 - Calculating Fuel Economy\n");
        printf("2 - Calculating Distance Travel\n");
        printf("3 - Revised Fuel Economy Calculation\n");
        printf("4 - Exit\n\n");
        printf("Choice feature: ");
        scanf("%d",&option);
        check = getchar();
        fpurge(stdin);
    }while(!checkChoice(option,check));
    printf("\n");
}

int checkPositiveInteger(int posInteger, char check) {
    //return 0 if user enter non-digit character
    if(check != '\n') {
        printf("This must be an integer. Please re-enter.\n");
        return 0;
    }
    //return 0 if user enter negative number
    if(posInteger <= 0) {
        printf("This must be greater than 0.\n");
        return 0;
    }
        return 1;
}

int checkPositiveDouble(double posDouble, char check) {
    //return 0 if user enter non-digit character
    if(check != '\n') {
        printf("This must be a double number. Please re-enter.\n");
        return 0;
    }
    //return 0 if user enter negative number
    if(posDouble <= 0) {
        printf("This must be greater than 0.\n");
        return 0;
    }
    if(posDouble > 10) {
        printf("This must be smaller than 10.\n");
        return 0;
    }
        return 1;
}

void inputMin(int &minute) {
    //repeat if input minute is invalid
    do {
        printf("How many minutes did you drive?\n");
        scanf("%d",&minute);
        check = getchar();
        fpurge(stdin);
    }while(!checkPositiveInteger(minute,check));
}

void inputSpeed(double &speed) {
    //repeat if input speed is invalid
    do {
        printf("What was the average speed (miles/hour) of the car during that time?\n");
        scanf("%lf",&speed);
        check = getchar();
        fpurge(stdin);
    }while(!checkPositiveDouble(speed,check));
}

void inputGas(double &gas) {
    //repeat if input gallon is invalid
    do {
        printf("How many gallons of gas did your car use?\n");
        scanf("%lf",&gas);
        check = getchar();
        fpurge(stdin);
    }while(!checkPositiveDouble(gas,check));
}

void inputRadius(double &radius) {
    //repeat if input radius is invalid
    do {
        printf("What is the radius of your tires, in inches?\n");
        scanf("%lf",&radius);
        check = getchar();
        fpurge(stdin);
    }while(!checkPositiveDouble(radius,check));
    //convert from inches to miles
    radius *= 1.0/63360;
}

void inputRevolutions(int &revolutions) {
    //repeat if input revolutions is invalid
    do {
        printf("How many revolutions did your car's tires make?\n");
        scanf("%d",&revolutions);
        check = getchar();
        fpurge(stdin);
    }while(!checkPositiveInteger(revolutions,check));
}

void fuelEco() {
    //range of time (minute)
    int minute;
    //average speed of car (miles/hour)
    double speed;
    //gasoline consume(gallon)
    double gas;
    inputMin(minute);
    inputSpeed(speed);
    inputGas(gas);
    printf("Your car averaged %.2lf miles per gallon.\n\n",((speed/60)*minute)/gas);
}

void distance() {
    //radius of tire
    double radius;
    //number of revolutions of tire
    int revolutions;
    inputRadius(radius);
    inputRevolutions(revolutions);
    printf("Your car traveled %.2lf miles.\n\n",2*radius*PI*revolutions);
}

void reFuelEco() {
    //radius of tire
    double radius;
    //number of revolutions of tire
    int revolutions;
    //gasoline consume(gallon)
    double gas;
    inputRadius(radius);
    inputRevolutions(revolutions);
    inputGas(gas);
    printf("Your car averaged %.2lf miles per gallon.\n\n",(2*radius*PI*revolutions)/gas);
}

int main() {
    printf("------------Fuel Economy-------------\n");
    //option of menu
    int option;
    do {
        displayMenu(option);
        switch(option) {
            case 1:
                fuelEco();
                break;
            case 2:
                distance();
                break;
            case 3:
                reFuelEco();
                break;
            case 4:
                printf("Thank for using!");
                break;
        }
    }while(option != 4);
}