#include<stdio.h>
#include<string.h>
#include<ctype.h>
#include<stdlib.h>
#include<math.h>

int checkSize(int size) {
    //if user enter more than 100 grade, user have to re-enter
    if (size > 100) {
        printf("You have entered more than 100 grades. Please re-enter.\n");
        return 0;
    }
    return 1;
}

int checkGrade(int grade, char check) {
    //if user enter a char, check will receive its value => check != \n, invalid
    if(check != '\n') {
        printf("Grade must be an integer. Please re-enter.\n");
        return 0;
    }
    //input < 0, > 100 is invalid
    if((grade < 0 || grade > 100) && grade != -999) {
        printf("Grade must be in range [0,100]\n");
        return 0;
    }
        return 1;
}

void enterMark(int *grade, int &size) {
    //char to check input
    char check;
    //repeat if user enter more than 100 grades
    do {
        size = 0;
        printf("Please enter the number:\n");
        //user enter grades
        for (int i = 0; grade[i-1] != -999; i++) {
            //repeat if user enter invalid grade
            do {
                scanf("%d",&grade[i]);
                check = getchar();
                fpurge(stdin);
            }while(!checkGrade(grade[i],check));
            //count the number of grade
            size++;
        }
        //remove -999 out of grade list
        size--;
    }while(checkSize(size)==0);
    
}

void frequency(int *grade,int size) {
    //allocate a 2-dimension string to display histogram
    char **histogram = (char**)malloc(21*sizeof(char*));
    //dynamic allocate row of histogram
    for (int i = 0; i < 21; i++) {
        histogram[i] = (char*)malloc(50*sizeof(char));
    }
    //initialize histogram
    strcpy(histogram[0],"0-4|");
    strcpy(histogram[1],"5-9|");
    strcpy(histogram[2],"10-14|");
    strcpy(histogram[3],"15-19|");
    strcpy(histogram[4],"20-24|");
    strcpy(histogram[5],"25-29|");
    strcpy(histogram[6],"30-34|");
    strcpy(histogram[7],"35-39|");
    strcpy(histogram[8],"40-44|");
    strcpy(histogram[9],"45-49|");
    strcpy(histogram[10],"50-54|");
    strcpy(histogram[11],"55-59|");
    strcpy(histogram[12],"60-64|");
    strcpy(histogram[13],"65-69|");
    strcpy(histogram[14],"70-74|");
    strcpy(histogram[15],"75-79|");
    strcpy(histogram[16],"80-84|");
    strcpy(histogram[17],"85-89|");
    strcpy(histogram[18],"90-94|");
    strcpy(histogram[19],"95-99|");
    strcpy(histogram[20],"100|");
    //count the appearance of grade by concat "*"
    for (int i = 0; i < 21; i++) {
        for (int j = 0; j < size; j++) {
            if(grade[j] >= (i*5) && grade[j] <= (i*5+4)) {
                strcat(histogram[i],"*");
            }
        }
    }
    printf("\n\nHere is a histogram of the adjusted data:\n\n");
    //display histogram after adjusted
    for (int i = 0; i < 21; i++) {
        puts(histogram[i]);
    }
    //re-allocate rows of histogram
    for (int i = 0; i < 21; i++) {
        histogram[i]=(char*)realloc(histogram[i],strlen(histogram[i])*sizeof(char));
    }
}

int imax(int *grade, int size) {
    //index of max grade
    int imax = 0;
    for (int i = 1; i < size; i++) {
        if (grade[i]>grade[imax]) imax = i;
    }
    return imax;
}

int imin(int *grade, int size) {
    //index of min grade
    int imin = 0;
    for (int i = 1; i < size; i++) {
        if (grade[i]<grade[imin]) imin = i;
    }
    return imin;
}

void removeAt(int *grade, int &size, int removeIndex) {
    //assign value of after element to pre-element
    for (int i = removeIndex; i < size-1; i++) {
        grade[i] = grade[i+1];
    }
    size--;
}

void removeMaxMin(int *grade, int &size) {
    int removeIndex = imin(grade,size);
    printf("\nThe data has been adjusted by removing the minimum %.2lf.",(double)grade[removeIndex]);
    removeAt(grade,size,removeIndex);
    removeIndex = imax(grade,size);
    printf("\nThe data has been adjusted by removing the minimum %.2lf.",(double)grade[removeIndex]);
    removeAt(grade,size,removeIndex);
}

double findMean(int *grade, int size) {
    double sum = 0;
    for (int i = 0; i < size; i++) {
        sum+=grade[i];
    }
    return sum/size;
}

double findStandardDeviation(int *grade, int size, double mean) {
    //array support calculating variance
    double *preVariance = (double*)malloc(size*sizeof(double));
    //variance to find standard deviation(= sqrt(variance))
    double variance;
    //sum of elements in above array after processed;
    double sumSquare = 0;
    //calculate sum of elements
    for (int i = 0; i < size; i++) {
        preVariance[i] = fabs(mean-grade[i])*fabs(mean-grade[i]);
        sumSquare+=preVariance[i];
    }
    variance = sumSquare/size;
    return sqrt(variance);
}

void DataAnalysis() {
    //array contains grades
    int *grade = (int*)malloc(101*sizeof(int));
    //number of marks user input
    int size;
    double mean, deviation;
    //step 1
    enterMark(grade,size);
    //step 2
    removeMaxMin(grade,size);
    grade = (int*)realloc(grade,size*sizeof(int));
    //step 3
    mean = findMean(grade,size);
    printf("\nThe adjusted mean is %.2lf",mean);
    //step 4
    deviation = findStandardDeviation(grade,size,mean);
    printf("\nThe adjusted deviation is %.2lf",deviation);
    //step 6
    frequency(grade,size);
}

int main() {
    printf("Data Analysis\n");
    printf("----------------------------------\n");
    DataAnalysis();
}