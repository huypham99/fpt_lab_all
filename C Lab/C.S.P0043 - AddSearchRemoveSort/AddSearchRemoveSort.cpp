#include<stdio.h>
#include<string.h>
#include<ctype.h>
#include<stdlib.h>
#include<math.h>

int checkSize(int size, char check) {
    if(check != '\n') {
        printf("Size must be an integer. Please re-enter.\n");
        return 0;
    }
    if(size <= 0) {
        printf("Size must be larger than 0.\n");
        return 0;
    }
        return 1;
}

int checkElement(int element, char check) {
    if(check != '\n') {
        printf("Element must be an integer. Please re-enter.\n");
        return 0;
    }
    return 1;
}

int checkOption(int option, char check) {
    if(check != '\n') {
        printf("Option must be a number. Please re-enter.\n");
        return 0;
    }
        return 1;
}

void displayMenu(int &option) {
    option = -2;
    printf("Choose one of the following options:");
    printf("\n1- Add a value");
    printf("\n2- Search a value");
    printf("\n3- Remove the first existence of a value");
    printf("\n4- Remove all existence of a value");
    printf("\n5- Print out the array");
    printf("\n6- Sort the array in ascending order");
    printf("\n7- Sort the array in descending order");
    printf("\nOthers- Quit\n");
    scanf("%d",&option);
    fpurge(stdin);
}

void addValue (int **array, int &size) {
    char check;
    int newValue;
    //repeat if input invalid
    do {
        printf("Please enter insert value: ");
        scanf("%d",&newValue);
        check = getchar();
        fpurge(stdin);
    }while(!checkElement(newValue,check));
    //increase size;
    size++;
    //temp pointer
    int *tempArr = NULL;
    //new pointer point to chunk memory of array(have been re-allocated)
    tempArr = (int*)realloc(*array,size*sizeof(int));
    tempArr[size-1]=newValue;
    //point to chunk memory of temp pointer
    *array = tempArr;
    printf("New value has been added!\n");
}

void searchValue(int *array, int size) {
    int searchValue;
    char check;
    //count the appearance of search value
    int count = 0;
    //repeat if user enter invalid value
    do {
        printf("Please enter search value: ");
        scanf("%d",&searchValue);
        check = getchar();
        fpurge(stdin);
    }while(!checkElement(searchValue,check));
    printf("Found index(es) of %d:",searchValue);
    //from first to last element in array
    for (int i = 0; i < size; i++) {
        //if input value appear in array
        if(searchValue==array[i]) {
            //output its position
            printf("\t%d",i);
            //increase number of occurrences
            count++;
        }
    }
    //if input number not in array, display notification
    if(count == 0) printf("\tNothing\nThe array does not contain your search value\n");
    else printf("\n");
}

void displayArray(int *array, int size) {
    printf("This is the current array:\n");
    for (int i = 0; i < size; i++) {
        printf("%d\t",array[i]);
    }
    printf("\n");
}

void removeAt(int *array, int &size, int removeIndex) {
    //from first to last element in array
    for(int i = removeIndex; i < size-1; i++) {
        //assign right value to left value
        array[i]=array[i+1];
    }
    //decrease size
    size--;
}

void removefirstEx(int *array, int &size) {
    //remove value user input
    int removeVal;
    //check input
    char check;
    //check removing process perform or not
    bool isRemove = false;
    //repeat if user enter invalid value
    do {
        printf("Please enter remove value: ");
        scanf("%d",&removeVal);
        check = getchar();
        fpurge(stdin);
    }while(!checkElement(removeVal,check));
    //from 1st to last element in array
    for (int i = 0; i < size; i++) {
        //if found input value in array
        if(removeVal == array[i]) {
            //remove it
            removeAt(array,size,i);
            isRemove = true;
            //stop looping, just remove first existence
            break;
        }
    }
    //if removing process have been performed, notice
    if(isRemove) printf("First existence of input value have been removed.\n");
    else printf("The array does not contain input value.\n");
}

void removeAllEx(int *array, int &size) {
    //remove value user input
    int removeVal;
    //check input
    char check;
    //check if removing process perform or not
    bool isRemove = false;
    //repeat if user enter invalid value
    do {
        printf("Please enter remove value: ");
        scanf("%d",&removeVal);
        check = getchar();
        fpurge(stdin);
    }while(!checkElement(removeVal,check));
    //from 1st to last element in array
    for (int i = 0; i < size ; ) {
        //if found input value in array
        if(removeVal == array[i]) {
            //remove it
            removeAt(array,size,i);
            isRemove = true;
        }else i++;
    }
    if(isRemove) printf("All existence of input value have been removed.\n");
    else printf("The array does not contain input value.\n");
}

void bubbleAsc(int array[], int size) {
    int i,j;
    //from first element to element before the last one
    for (i = 0; i < size-1; i++) {
        //from last element to i-th element
        for (j = size - 1; j > i; j--) {
            //if the element is smaller than left element, swap them (push min to left)
            if(array[j]<array[j-1]) {
                int temp = array[j];
                array[j] = array[j-1];
                array[j-1] = temp;
            }
        }
    }
}

void sortingAsc(int *array, int size) {
    bubbleAsc(array,size);
    printf("Your array have been sorted in ascending order.\n");
}

void bubbleDesc(int array[], int size) {
    int i,j;
    //from first element to element before the last one
    for (i = 0; i < size-1; i++) {
        //from last element to i-th element
        for (j = size - 1; j > i; j--) {
            //if the element is greater than left element, swap them (push max to left)
            if(array[j]>array[j-1]) {
                int temp = array[j];
                array[j] = array[j-1];
                array[j-1] = temp;
            }
        }
    }
}

void sortingDesc(int *array, int size) {
    bubbleDesc(array,size);
    printf("Your array have been sorted in descending order.\n");
}

int main() {
    printf("------Array Manipulations 1------\n");
    //input array will be processed
    int *array = (int*)malloc(1*sizeof(int));
    //size of array
    int size = 0;
    //option of menu
    int option;
    //repeat while user enter option 1 to 7
    do {
//        //system pause
//        system("read -n 1 -s -p\"Press any key to display menu...\n\"");
        displayMenu(option);
        //process option of user
        switch(option) {
            case 1:
                addValue(&array,size);
                break;
            case 2:
                searchValue(array,size);
                break;
            case 3:
                removefirstEx(array,size);
                array = (int*)realloc(array,size*sizeof(int));
                break;
            case 4:
                removeAllEx(array,size);
                array = (int*)realloc(array,size*sizeof(int));
                break;
            case 5:
                displayArray(array,size);
                break;
            case 6:
                sortingAsc(array,size);
                break;
            case 7:
                sortingDesc(array,size);
                break;
            default:
                printf("Thanks for using!");
        }
    }while(option >= 1 && option <= 7);
}