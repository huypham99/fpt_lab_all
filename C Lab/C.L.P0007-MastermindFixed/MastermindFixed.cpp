/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: MSI
 *
 * Created on November 11, 2018, 12:50 PM
 */

#include <cstdlib>
#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>


bool match(char *string) {
    if(strlen(string)!=7) {
        printf("Input must be in format \"x x x x\" (0 <= x <= 5, x is integer).\n");
        return false;
    }
    for (int i = 0; i < 7; i++) {
        if((i % 2==0 && !isdigit(string[i])) || (i%2==1&& string[i]!=' ')) {
            printf("Input must be in format \"x x x x\" (0 <= x <= 5, x is integer).\n");
            return false;
        }
    }
    bool exceeded= false;
    for (int i = 0; i < 4; i++) {
        if(string[2*i] > '5') {
            printf("Number at position %d is larger than 5.\n",i+1);
            exceeded = true;
        }
    }
    if(exceeded) return false;
    return true;
}

void input(char *inputString, int *inputArr) {
    do {
        gets(inputString);
    }while(!match(inputString));
    inputString = (char*)realloc(inputString,7*sizeof(char));
    //convert
    for(int i = 0; i < 4; i++) {
        inputArr[i] = inputString[2*i]-'0';
    }
}

void initOb(int *objectiveArr) {
    srand(time(NULL));
    for (int i = 0; i < 4; i++) {
        objectiveArr[i]=rand()%6;
    }
    printf("Winning board:");
    for (int i = 0; i < 4; i++) {
        printf(" %d",objectiveArr[i]);
    }
    printf("\n");
}

void removeAt(int *arr, int &size, int k) {
    for(int i = k; i < size-1; i++) {
        arr[i]=arr[i+1];
    }
    size--;
}

void compare(int *inputArr, int *objectiveArr, bool &isWin) {
    int countIm = 0;
    int countPer = 0;
    int cloneOb[4];
    int sizeClone = 4;
    for (int i = 0; i < 4; i++) {
        cloneOb[i] = objectiveArr[i];
    }
    //count imperfect+perfect
    for (int i = 0; i < 4; i++) {
        for(int j = 0; j < sizeClone; j++) {
            if(inputArr[i]==cloneOb[j]) {
                countIm++;
                removeAt(cloneOb,sizeClone,j);
                break;
            }
        }
    }
    //count perfect
    for (int i = 0; i < 4; i++) {
        if(inputArr[i]==objectiveArr[i]) countPer++;
    }
    countIm-=countPer;
    printf("You have %d perfect matches and %d imperfect matches.\n",countPer, countIm);
    if(countPer==4) isWin = true;
}

void instruction() {
    printf("Welcome to Master Mind\n\n");
    printf("At each turn, you will enter your guess for the playing board.\n");
    printf("Each guess will have each number within the guess separated by a space.\n");
    printf("When you are ready, enter your first guess.\n");
    printf("From that point on, you will be told how many perfect and imperfect matches you have.\n");
    printf("After this message, you should guess again. You have 10 chances, good luck!\n\n");
}

int main() {
    instruction();
    time_t start_t, end_t;
    time(&start_t);
    char *inputString=(char*)malloc(50*sizeof(char));
    int inputArr[4];
    int times = 0;
    bool isWin = false;
    int objectiveArr[4]={5,1,4,5};
    initOb(objectiveArr);
    while(true) {
        input(inputString,inputArr);
        times++;
        compare(inputArr,objectiveArr,isWin);
        if(times==10 && !isWin) {
            printf("Sorry, you have exceeded the maximum number of turns. You lose.\n");
            printf("Here is the winning board:");
            for (int i = 0; i < 4; i++) {
                printf(" %d",objectiveArr[i]);
            }
            break;
        }
        if(isWin) {
            time(&end_t);
            int second = (int) difftime(end_t, start_t);
            printf("You have won the game in %d rounds and %d:%d!!",times,second/60,second%60);
            break;
        }
    }
    return 0;
}