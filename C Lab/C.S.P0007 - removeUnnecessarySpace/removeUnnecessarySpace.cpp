#include <cstdlib>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

int checkInput(char *string) {
    //int to count number of space
    int countBlank=0;
    //from first to last element of the string
    for (int i = 0; i < strlen(string); i++) {
        //count the number of space
        if (isspace(string[i])) countBlank++;
        //return 0 if there is a special character in string
        if (string[i]=='@'||string[i]=='#'||string[i]=='!'||string[i]=='['||string[i]==']'||string[i]=='{'||string[i]=='}'
            || string[i]==')'||string[i]=='(') {
            printf("String cannot contains special character!\n");
            return 0;
        }
    }
    //if the whole string is blank, return 0
    if (countBlank==strlen(string)) {
        printf("String cannot be blank!\n");
        return 0;
    }
    return 1;
}
 
void enterString(char *inputString) {
    //repeat if input string is blank or contains special character
    do {
        printf("Please enter a string: ");
        gets(inputString);
    }while(!checkInput(inputString));
}

void removeAt(char *string, int indexRemove) {
    int i;
    //remove value at wanted index (actually copy after value to pre-value)
    for (i = indexRemove; i < strlen(string); i++) {
        string[i]=string[i+1];
    }
}

void removeUnnecessarySpace(char *inputString) {
    //the string after remove all unnecessary space
    char *displayString=(char*)malloc(100*sizeof(char));
    //i: index of inputString; j: index of displayString
    int i = 0, j = 0;
    //from first to last letter in input string
    while (i < strlen(inputString)) {
        /*push i while there is a blank at the right of a blank (remove all unnecessary string 
        except the blank which is at the left of a letter) */
        while (inputString[i]==' ' && inputString[i+1]==' ') {
            i++;
        }
        //assign letter in input string to display string
        displayString[j++]=inputString[i++];
    }
    //assign NULL to element right after last letter
    displayString[j]='\0';
    //remove space at the start of display string
    if(displayString[0]==' ') removeAt(displayString,0);
    //remove space at the end of display string
    if(displayString[j-1]==' ') removeAt(displayString,j-1);
    //copy display string to input string
    strcpy(inputString,displayString);
}

int checkExit(char exit, char check) {
    //if user enter more than 1 character => check != ENTER, return 0
    if (check != '\n') {
        printf("Please enter no more than 1 letter!\n");
        return 0;
    }
    //if user enter a character != 'y' and 'n'
    if (exit != 'y' && exit != 'n') {
        printf("Please just enter 'y' or 'n'!\n");
        return 0;
    }
    return 1;
}

void exitOrCont(char &exit) {
    char check;
    //repeat if user enter != 'y' and 'n'
    do {
        printf("Enter 'y' to continue, 'n' to exit.\n");
        exit = getchar();
        check = getchar();
        fpurge(stdin);
    }while(!checkExit(exit,check));
}

int main() {
    printf("Program to remove all necessary space in input string.\n");
    printf("---------------------------------------------\n");
    char exit;
    //repeat when user want to continue (exit == 'y')
    do {
        //input string of user
        char *inputString = (char*)malloc(100*sizeof(char));
        enterString(inputString);
        removeUnnecessarySpace(inputString);
        //re-allocate input string
        inputString = (char*)realloc(inputString,strlen(inputString)*sizeof(char));
        printf("The string after removing: ");
        puts(inputString);
        exitOrCont(exit);
    }while(exit == 'y');
}
