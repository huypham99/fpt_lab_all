#include<stdio.h>
#include<string.h>
#include<ctype.h>
#include<stdlib.h>

removeAt(int *a, int &n, int removeIndex) {
    for (int i = removeIndex; i < n; i++) {
        a[i] = a[i + 1];
    }
    n--;
}

void removeDup(int *a, int &n) {
//    for (int i = n - 1; i >= 1; i--) {
//        for (int j = i - 1; j >= 0 ; j--) {
//            if (a[i]==a[j]) {
//                removeAt(a,n,j);
//            }
//        }
//    }
    for (int i = 0; i < n-1; i++) {
        for (int j = i + 1; j < n; j++) {
            if (a[i]==a[j]) {
                removeAt(a,n,j);
                j--;
            }
        }
    }
}

int main() {
    int a[100];
    int n;
    printf("n = "); scanf("%d",&n);
    for (int i = 0; i < n; i++) {
        printf("a[%d] = ",i); scanf("%d",&a[i]);
    }
    for (int i = 0; i < n; i++) {
        printf("%d\t",a[i]);
    }
    removeDup(a,n);
    printf("\n");
    for (int i = 0; i < n; i++) {
        printf("%d\t",a[i]);
    }
}