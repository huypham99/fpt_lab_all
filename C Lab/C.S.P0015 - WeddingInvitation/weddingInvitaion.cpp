#include<stdio.h>
#include<string.h>
#include<ctype.h>
#include<stdlib.h>
#include<math.h>

int checkPositiveInteger(int posInteger, char check) {
    //return 0 if user enter non-digit character
    if(check != '\n') {
        printf("This must be an integer. Please re-enter.\n");
        return 0;
    }
    //return 0 if user enter negative number
    if(posInteger <= 0) {
        printf("This must be greater than 0.\n");
        return 0;
    }
        return 1;
}

int checkPositiveDouble(double posDouble, char check) {
    //return 0 if user enter non-digit character
    if(check != '\n') {
        printf("This must be a double number. Please re-enter.\n");
        return 0;
    }
    //return 0 if user enter negative number
    if(posDouble <= 0) {
        printf("This must be greater than 0.\n");
        return 0;
    }
        return 1;
}

int checkLarge(double largeCost, double smallCost, char check) {
    //return 0 if user enter non-digit character
    if(check != '\n') {
        printf("This must be a double number. Please re-enter.\n");
        return 0;
    }
    //return 0 if user enter negative number
    if(largeCost <= 0) {
        printf("This must be greater than 0.\n");
        return 0;
    }
    if(largeCost <= smallCost) {
        printf("A large package must be more expensive than a small package. Please re-enter.\n");
        return 0;
    }
        return 1;
}

void input(double &smallCost, double &largeCost, int &invitations) {
    //check input
    char check;
    //repeat if input is invalid
    do {
        printf("What is the cost of a small package (in dollars)?\n");
        scanf("%lf",&smallCost);
        check = getchar();
        fpurge(stdin);
    }while(!checkPositiveDouble(smallCost,check));
    //repeat if input is invalid
    do {
        printf("What is the cost of a large package (in dollars)?\n");
        scanf("%lf",&largeCost);
        check = getchar();
        fpurge(stdin);
    }while(!checkLarge(largeCost,smallCost,check));
    //repeat if input is invalid
    do {
        printf("How many invitations are you sending out?\n");
        scanf("%d",&invitations);
        check = getchar();
        fpurge(stdin);
    }while(!checkPositiveInteger(invitations,check));
    printf("\n");
}

double ceil(double number) {
    double newNum;
    if (number == (int)number) newNum = number;
    else if (number > 0) {
        newNum = number + (1 - (number - (int)number));
    }else {
        newNum = (int)number;
    }
    return newNum;
}

double minimizeCost(double smallCost, double largeCost, int invitations, int &smallQuantity, int &largeQuantity) {
    
    double minCost;
    if (invitations <= 50) {
        smallQuantity = 1;
        largeQuantity = 0;
        return smallQuantity*smallCost+largeQuantity*largeCost;
    }
    if(invitations % 50 != 0) invitations += (50 - (invitations % 50));
    smallQuantity = invitations / 50;
    largeQuantity = 0;
    while(((smallQuantity-4)*smallCost+(largeQuantity+1)*largeCost)<=(smallQuantity*smallCost+largeQuantity*largeCost) && (smallQuantity-4)>=0) {
        smallQuantity-=4;
        largeQuantity++;
    }
    return smallQuantity*smallCost+largeQuantity*largeCost;
    }

void output(int smallQuantity, int largeQuantity, double minCost) {
    printf("You should order %d small package(s)\n",smallQuantity);
    printf("You should order %d large package(s)\n",largeQuantity);
    printf("Your cost for invitations will be $%.2lf",minCost);
}

int main() {
    printf("--------------------Wedding Invitation----------------------\n");
    //cost of a small package
    double smallCost;
    //cost of a large package
    double largeCost;
    //whole number of invitations
    int invitations;
    //number of small package should be bought
    int smallQuantity;
    //number of large package should be bought
    int largeQuantity;
    input(smallCost,largeCost,invitations);
    //minimum cost output
    double minCost = minimizeCost(smallCost,largeCost,invitations,smallQuantity,largeQuantity);
    output(smallQuantity,largeQuantity,minCost);
}

