#include <cstdlib>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <math.h>

int checkInput(char inputString[]) {
    int countBlank = 0;
    //from first to last letter in input string
    for (int i= 0; i < strlen(inputString); i++) {
        //if there is a space char, increase countBlank
        if(inputString[i] == ' ') countBlank++;
    }
    //if the whole string is blank, return 0
    if (countBlank == strlen(inputString)) {
        printf("String cannot blank.");
        return 0;
    }
    //else return 1
    return 1;
}

void input(char inputString[]) {
    //repeat if string is invalid
    do {
        printf("Please enter string: ");
        gets(inputString);
    }while(checkInput(inputString)==0);
}

void split(char inputString[], char **splitString, int &wordNum) {
    //i, j: index of 2-dimension string; k: index of inputString[]
    int i = 0, j = 0, k = 0;
    wordNum = 0;
//    //delete all 
//    while(1) {
//        if(inputString[k] == ' ' || inputString[k]=='_') k++;
//        else break;
//    }
    //break after reach last letter in inputString
    while (k < strlen(inputString)) {
        //initialize value of corresponding letter in inputString[] to splitString[][]
        splitString[i][j] = inputString[k];
        k++;
        j++;
        //if value of inputString[k] is space or underscore or NULL
        if (inputString[k] == ' ' || inputString[k]== '_' || k == strlen(inputString)) {
            //init NULL after last letter in current word.
            splitString[i][j] = '\0';
            //start new word in splitString[][]
            i++;
            //increase number of word
            wordNum++;
            //reset index of new word
            j = 0;
            //space or underscore as a new word
            splitString[i][j] = inputString[k];
            //init NULL after "space or under score 'word'"
            splitString[i][j+1] = '\0';
            //start new word
            i++;
            //increase number of word
            wordNum++;
            //reset index of new word
            j = 0;
            //increase k
            k++;
        }
    }
    wordNum--;
}

void swap(char string1st[],char string2nd[]) {
    char *temp = (char*)malloc(20*sizeof(char));
    strcpy(temp,string1st);
    strcpy(string1st,string2nd);
    strcpy(string2nd,temp);
}

void reverse(char inputString[], char **splitString, int &wordNum) {
    split(inputString,splitString,wordNum);
    //index of first word in splitString
    int i = 0;
    //index of last word in splitString
    int j = wordNum-1;
    //swap corresponding word in splitString while index i < index j
    while(i<j) {
        swap(splitString[i],splitString[j]);
        i++;
        j--;
    }
}

void display(char inputString[], char **splitString, int wordNum) {
    printf("The old string: ");
    //print inputString
    puts(inputString);
    printf("The reversed string: ");
    //from first to last word in splitString
    for(int i = 0; i < wordNum; i++) {
        //print words
        printf("%s",splitString[i]);
    }
}

int checkTrigger(char triggerKey[]) {
    //if trigger key is not enter or ESCm return 0
    if (strcmp(triggerKey,"yes")!=0 && strcmp(triggerKey,"no")!=0) {
        printf("Please press again, pressed key just can be Enter or ESC");
        return 0;
    }
    //else return 1
    return 1;
}

void ReverseStringProgram() {
    //trigger char[] to verify whether continue program or not
    char *triggerKey = (char*)malloc(100*sizeof(char));
    //repeat if trigger key is 'yes'
    do {
        //input string which user enter
        char *inputString = (char*)malloc(100*sizeof(char));
        //2-dimension string used to reverse inputString (number of words - side x)
        char **splitString = (char**)malloc(30*sizeof(char*));
        //dynamic allocate number of character in each word.(side y)
        for (int i = 0; i < 30; i++) {
            splitString[i] = (char*)malloc(20*sizeof(char));
        }
        //number of word in input string
        int wordNum;
        input(inputString);
        //re-allocate inputString
        inputString = (char*)realloc(inputString,strlen(inputString)*sizeof(char));
        reverse(inputString,splitString,wordNum);
        //re-allocate side y
        for (int i = 0; i < 30; i++) {
            splitString[i] = (char*)realloc(splitString[i],strlen(splitString[i])*sizeof(char));
        }
        //reallocate side x
        splitString = (char**)realloc(splitString,wordNum*sizeof(char*));
        display(inputString,splitString,wordNum);
        //repeat if trigger key is not 'yes' and 'no'
        do {
            printf("\nEnter 'yes' to continue another reverse, 'no' to exit. ");
            gets(triggerKey);
            fpurge(stdin);
        }while(checkTrigger(triggerKey)==0);
        //re-allocate size of triggerKey[]
        triggerKey=(char*)realloc(triggerKey,strlen(triggerKey)*sizeof(char));
    }while(strcmp(triggerKey,"yes")==0);
}

int main() {
    printf("Program reverse words in string.\n");
    printf("------------------------------------------\n");
    ReverseStringProgram();
}