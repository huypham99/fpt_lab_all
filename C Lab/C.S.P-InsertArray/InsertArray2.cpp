#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <math.h>

int checkSize(int size, char check) {
    if(check != '\n') {
        printf("Size must be an integer. Please re-enter.\n");
        return 0;
    }
    if(size <= 0) {
        printf("Size must be larger than 0\n");
        return 0;
    }
        return 1;
}

int checkElement(int element, char check) {
    if(check != '\n') {
        printf("Size must be an integer. Please re-enter.\n");
        return 0;
    }
    return 1;
}


void createArray(int array[], int &size) {
    char check;
    //validate date type of array
    do {
        printf("Please enter size of array: ");
        scanf("%d",&size);
        check = getchar();
        fflush(stdin);
    }while(checkSize(size,check) == 0);
    //input array
    for (int i = 0; i < size; i++) {
    	//validate elements in array
        do {
            printf("Enter element[%d]: ",i);
            scanf("%d",&array[i]);
            check=getchar();
            fflush(stdin);
        }while(checkElement(array[i],check) == 0);
    }
}

void bubbleAsc(int array[], int size) {
    int i,j;
    //from first element to before the last one
    for (i = 0; i < size-1; i++) {
        //from last element to i-th element
        for (j = size - 1; j > i; j--) {
            //check if there is an element smaller than left element, swap them (like bubble rise to surface)
            if(array[j]<array[j-1]) {
                int temp = array[j];
                array[j] = array[j-1];
                array[j-1] = temp;
            }
        }
    }
}

void displaySortedArray(int array[],int size) {
    bubbleAsc(array,size);
    printf("The array after sorting:\n");
    for (int i = 0; i < size; i++) {
        printf("%d\t",array[i]);
    }
}

void insertAt(int array[], int &size, int newValue, int indexAdd) {
	if(indexAdd < 0 || indexAdd > size) return;
	for (int i = size; i > indexAdd; i--) {
		array[i] = array[i - 1];
	}
	array[indexAdd] = newValue;
	size++;
}

void addAndDisplay(int newValue, int array[],int &size) {
    //size before process
    int beforeSize = size;
    array = (int*)realloc(array,(size+1)*sizeof(int));
    //find true position of new value in array
    for (int i = size-1; i >= 0; i--) {
        if(newValue <= array[i]) {
            insertAt(array,size,newValue,i+1);
            break;
        }
    }
    //if size not change, insert at begin of array
    if (size==beforeSize) insertAt(array,size,newValue,0);
    printf("New array:\n");
    for (int i = 0; i < size; i++) {
        printf("%d\t",array[i]);
    }
}

void enterNewValue(int &newValue) {
    char check;
    printf("\n");
    do {
        printf("Please enter new value: ");
        scanf("%d",&newValue);
        check = getchar();
        fflush(stdin);
    }while(!checkElement(newValue,check));
}

int main() {
    //input array
    int *array = (int*)malloc(100*sizeof(int));
    //size of array
    int size;
    printf("Program to insert new element to an existing array\n");
    printf("---------------------------------------------------\n");
    createArray(array,size);
    array = (int*)realloc(array,size*sizeof(int));
    displaySortedArray(array,size);
    array = (int*)realloc(array,size*sizeof(int));
    //new value will be inserted
    int newValue;
    enterNewValue(newValue);
    array = (int*)realloc(array,size*sizeof(int));
    addAndDisplay(newValue,array,size);
}
